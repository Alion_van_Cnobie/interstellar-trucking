<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Update login and password</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/login.css" />" type="text/css">
</head>
<body>
<form:form id="new" class = "form" method="post" action="/pilot/confirm" modelAttribute="credentials">
    <div>
        <form:errors path="username" cssClass="error"/>
        <form:input class = "input"  path="username" autocomplete="off" type="text" placeholder="New username"/>
    </div>
    <div>
        <form:errors path="password" cssClass="error"/>
        <form:input class = "input" path="password" autocomplete="off" type="text" name="last name" placeholder="New password"/>
    </div>
    <div>
        <form:errors path="confirm" cssClass="error"/>
        <form:input class = "input" path="confirm" autocomplete="off" type="text" name="last name" placeholder="Confirm password"/>
    </div>
    <div>
        <button type="submit" form="new" class = "input">Submit</button>
    </div>
</form:form>
</body>
</html>