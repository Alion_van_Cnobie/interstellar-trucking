<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Logistician</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/logistician.css" />" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/table.css" />" type="text/css">
    <script src="<c:url value="/resources/js/utils.js" />"></script>
</head>
<body>
<div class="main">
    <%@include file="templates/navigation.jsp"%>
    <c:choose>
        <c:when test="${formEnabled == true}">
            <div class="center hidden" id="show">
                <%@include file="templates/trucks-table.jsp"%>
            </div>
            <div class="center" id="panel">
                <%@include file="templates/new-truck-form.jsp"%>
            </div>
        </c:when>

        <c:otherwise>
            <div class="center" id="show">
                <%@include file="templates/trucks-table.jsp"%>
            </div>
            <div class="center hidden" id="panel">
                <%@include file="templates/new-truck-form.jsp"%>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>

