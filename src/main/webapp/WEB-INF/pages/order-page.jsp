<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Logistician</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/pilot.css" />" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/table-unit.css" />" type="text/css">
    <script src="<c:url value="/resources/js/utils.js" />"></script>
</head>
<body>
<div class="main">
    <%@include file="templates/navigation.jsp"%>
    <div class="center">
        <div class="table">
            <div class="row">
                <div>Number:</div>
                <div>${order.number}</div>
            </div>
            <div class="row">
                <div>Start:</div>
                <div>${order.start}</div>
            </div>
            <div class="row">
                <div>Current:</div>
                <div>${order.current}</div>
            </div>
            <div class="row">
                <div>Finish:</div>
                <div>${order.finish}</div>
            </div>
            <div class="row">
                <div>Status:</div>
                <div>${order.status}</div>
            </div>
            <div class="row">
                <div>Truck:</div>
                <div>${order.truck}</div>
            </div>
            <div class="row">
                <div>Duration:</div>
                <div>${order.estimatedTime} hours</div>
            </div>
            <div class="row">
                <div>Max load:</div>
                <div>${order.maxLoad} tons</div>
            </div>
        </div>
    </div>
    <div class="center">
        <div class="table">
            <c:forEach var="checkpoint" items="${checkpoints}">
                <c:choose>
                    <c:when test="${checkpoint.position == order.currentPosition}">
                        <div class="row error">
                            <div>${checkpoint.location}</div>
                            <div>${checkpoint.type}</div>
                            <c:choose>
                                <c:when test="${checkpoint.cargo ne null}">
                                    <div>${checkpoint.cargo.name}</div>
                                    <div>${checkpoint.cargo.number}</div>
                                </c:when>
                                <c:otherwise>
                                    <div></div>
                                    <div></div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div>${checkpoint.location}</div>
                            <div>${checkpoint.type}</div>
                            <c:choose>
                                <c:when test="${checkpoint.cargo ne null}">
                                    <div>${checkpoint.cargo.name}</div>
                                    <div>${checkpoint.cargo.number}</div>
                                </c:when>
                                <c:otherwise>
                                    <div></div>
                                    <div></div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>

