<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Orders</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/logistician.css" />" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/table.css" />" type="text/css">
</head>
<body>
<div class="main">
    <%@include file="templates/navigation.jsp"%>
    <div class="center">
        <div class="container">
            <div id="table">
                <div class="row" id="header-row">
                    <div>Number</div>
                    <div>Start</div>
                    <div>Current</div>
                    <div>Finish</div>
                    <div>Status</div>
                    <div>Truck</div>
                    <div>Duration</div>
                    <div>Max load</div>
                </div>
                <c:forEach var="order" items="${orderList}">
                    <a href="/logistician/orders/${order.number}" class="row">
                        <div>${order.number}</div>
                        <div>${order.start}</div>
                        <div>${order.current}</div>
                        <div>${order.finish}</div>
                        <div>${order.status}</div>
                        <div>${order.truck}</div>
                        <div>${order.estimatedTime} h</div>
                        <div>${order.maxLoad} tons</div>
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</body>
</html>

