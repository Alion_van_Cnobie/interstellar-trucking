<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Logistician</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/pilot.css" />" type="text/css">
    <link rel="stylesheet" href="<c:url value="/resources/css/table-unit.css" />" type="text/css">
</head>
<body>
<div class="main">
    <%@include file="templates/navigation-pilot.jsp"%>
    <div class="center">
        <div class="table">
            <div class="row">
                <div>${pilotInfo.currentPilot.firstName} ${pilotInfo.currentPilot.lastName}</div>
                <div>${pilotInfo.currentPilot.number}</div>
            </div>
            <div class="row">
                <div>Location:</div>
                <div>${pilotInfo.currentPilot.location}</div>
            </div>
            <div class="row">
                <div>Status:</div>
                <div>${pilotInfo.currentPilot.status}</div>
            </div>
            <div class="row">
                <div>Truck:</div>
                <div>${pilotInfo.currentPilot.truck}</div>
            </div>
            <div class="row">
                <div>Order:</div>
                <div>${pilotInfo.orderNumber}</div>
            </div>
        </div>
    </div>
    <div class="center">
        <div class="table">
            <c:forEach var="pilot" items="${pilotInfo.copilots}">
                <c:if test="${pilot.number ne pilotInfo.currentPilot.number}">
                    <div class="row">
                        <div>${pilot.firstName} ${pilot.lastName}</div>
                        <div>${pilot.number}</div>
                    </div>
                </c:if>
            </c:forEach>
        </div>
    </div>
    <div class="center">
        <div class="table">
            <c:forEach var="checkpoint" items="${pilotInfo.checkpoints}">
                <c:choose>
                    <c:when test="${checkpoint.id == pilotInfo.current.id}">
                        <div class="row error">
                            <div>${checkpoint.location}</div>
                            <div>${checkpoint.type}</div>
                            <c:choose>
                                <c:when test="${checkpoint.cargo ne null}">
                                    <div>${checkpoint.cargo.name}</div>
                                    <div>${checkpoint.cargo.number}</div>
                                </c:when>
                                <c:otherwise>
                                    <div></div>
                                    <div></div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div>${checkpoint.location}</div>
                            <div>${checkpoint.type}</div>
                            <c:choose>
                                <c:when test="${checkpoint.cargo ne null}">
                                    <div>${checkpoint.cargo.name}</div>
                                    <div>${checkpoint.cargo.number}</div>
                                </c:when>
                                <c:otherwise>
                                    <div></div>
                                    <div></div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>

