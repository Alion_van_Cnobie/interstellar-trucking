<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Login page</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/login.css" />" type="text/css">
</head>
<body>
<c:url value="/login" var="loginUrl"/>
<form class = "form" action="${loginUrl}" method="post">
    <div>
        <input class = "input" autocomplete="off" type="text" name="username" placeholder="Username" required minlength="3">
    </div>
    <div>
        <input class = "input" type="password" placeholder="Password" name="password" required minlength="1">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </div>
    <div>
        <button type="submit" class = "input">Login</button>
    </div>
</form>
</body>
</html>