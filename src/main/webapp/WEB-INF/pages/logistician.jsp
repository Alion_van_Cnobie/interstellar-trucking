<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Logistician</title>
    <link rel="stylesheet" href="<c:url value="/resources/css/logistician.css" />" type="text/css">
</head>
<body>
<div class="main">
    <%@include file="templates/navigation.jsp"%>
    <div class="center"></div>
</div>
</body>
</html>
