<div id="table">
    <div class="row">
        <div>Number:</div>
        <div>${truck.number}</div>
    </div>
    <div class="row">
        <div>Shift size:</div>
        <div>${truck.shiftSize}</div>
    </div>
    <div class="row">
        <div>Capacity:</div>
        <div>${truck.capacity}</div>
    </div>
    <div class="row">
        <div>Status:</div>
        <div>${truck.status}</div>
    </div>
    <div class="row">
        <div>Location:</div>
        <div>${truck.location}</div>
    </div>
</div>
<h3>Are you sure?</h3>
<div class="ok">
    <button form="deleteForm" type="submit">Yes</button>
</div>
<div class="cancel">
    <button onclick="hideDelete()">No</button>
</div>
<form:form id="deleteForm" method="post" action="/logistician/trucks/${truck.number}/delete">
</form:form>