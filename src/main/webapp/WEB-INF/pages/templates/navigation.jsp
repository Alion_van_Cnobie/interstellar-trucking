<div class="left">
    <div>
        <button form="pilots" type="submit">Pilots</button>
    </div>
    <div>
        <button form="trucks" type="submit">Trucks</button>
    </div>
    <div>
        <button form="cargoes" type="submit">Cargoes</button>
    </div>
    <div>
        <button form="orders" type="submit">Orders</button>
    </div>
    <div>
        <form id="logoutForm" method="post" action="/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <button type="submit" class="logout">Logout</button>
        </form>
    </div>
    <form id="pilots" method="get" action="/logistician/pilots">
    </form>
    <form id="trucks" method="get" action="/logistician/trucks">
    </form>
    <form id="cargoes" method="get" action="/logistician/cargoes">
    </form>
    <form id="orders" method="get" action="/logistician/orders">
    </form>
</div>