<form:form id="update" method="post" action="/logistician/trucks/${truck.number}/update" modelAttribute="truckUpd">
    <div>
        <form:errors path="number" cssClass="error"/>
        <form:input  path="number" autocomplete="off" type="text" value="${truck.number}"/>
    </div>
    <div>
        <form:errors path="shiftSize" cssClass="error"/>
        <form:input path="shiftSize" autocomplete="off" type="number" value="${truck.shiftSize}"/>
    </div>
    <div>
        <form:errors path="capacity" cssClass="error"/>
        <form:input path="capacity" autocomplete="off" type="number" value="${truck.capacity}"/>
    </div>
    <div>
        <form:select path="status">
            <c:forEach var="status" items="${statuses}">
                <c:choose>
                    <c:when test="${truck.status == status}">
                        <option value="${status}" selected>${status}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${status}">${status}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
    <div>
        <form:select path="location">
            <c:forEach var="location" items="${locations}">
                <c:choose>
                    <c:when test="${truck.location == location}">
                        <option value="${location}" selected>${location}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${location}">${location}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button form="update" type="submit">Update</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>