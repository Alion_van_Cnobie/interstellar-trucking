<div id="table">
    <div class="row" id="header-row">
        <div>Number</div>
        <div>Shift size</div>
        <div>Capacity</div>
        <div>Status</div>
        <div>Location</div>
    </div>
    <c:forEach var="truck" items="${truckList}">
        <a href="/logistician/trucks/${truck.number}" class="row">
            <div>${truck.number}</div>
            <div>${truck.shiftSize}</div>
            <div>${truck.capacity} tons</div>
            <div>${truck.status}</div>
            <div>${truck.location}</div>
        </a>
    </c:forEach>
</div>
<div class="ok">
    <button onclick="showForm()">New truck</button>
</div>
