<div id="table">
    <div class="row">
        <div>Number:</div>
        <div>${cargo.number}</div>
    </div>
    <div class="row">
        <div>Name:</div>
        <div>${cargo.name}</div>
    </div>
    <div class="row">
        <div>Weight:</div>
        <div>${cargo.weight}</div>
    </div>
    <div class="row">
        <div>Load location:</div>
        <div>${cargo.start}</div>
    </div>
    <div class="row">
        <div>Unload location:</div>
        <div>${cargo.end}</div>
    </div>
    <div class="row">
        <div>Status:</div>
        <div>${cargo.status}</div>
    </div>
</div>
<h3>Are you sure?</h3>
<div class="ok">
    <button form="deleteForm" type="submit">Yes</button>
</div>
<div class="cancel">
    <button onclick="hideDelete()">No</button>
</div>
<form:form id="deleteForm" method="post" action="/logistician/cargoes/${cargo.number}/delete">
</form:form>