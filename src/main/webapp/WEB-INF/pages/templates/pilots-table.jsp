<div id="table">
    <div class="row" id="header-row">
        <div>Name</div>
        <div>Private number</div>
        <div>Location</div>
        <div>Status</div>
        <div>Truck</div>
        <div>Worked out</div>
    </div>
    <c:forEach var="pilot" items="${pilotList}">
        <a href="/logistician/pilots/${pilot.number}" class="row">
            <div>${pilot.firstName} ${pilot.lastName}</div>
            <div>${pilot.number}</div>
            <div>${pilot.location}</div>
            <div>${pilot.status}</div>
            <div>${pilot.truck}</div>
            <div>${pilot.workedOut} hours</div>
        </a>
    </c:forEach>
</div>
<div class="ok">
    <button onclick="showForm()">New pilot</button>
</div>
