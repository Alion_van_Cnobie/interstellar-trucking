<div class="table">
    <div class="row">
        <div>Number:</div>
        <div>${cargo.number}</div>
    </div>
    <div class="row">
        <div>Name:</div>
        <div>${cargo.name}</div>
    </div>
    <div class="row">
        <div>Weight:</div>
        <div>${cargo.weight} tons</div>
    </div>
    <div class="row">
        <div>Load location:</div>
        <div>${cargo.start}</div>
    </div>
    <div class="row">
        <div>Unload location:</div>
        <div>${cargo.end}</div>
    </div>
    <div class="row">
        <div>Status:</div>
        <div>${cargo.status}</div>
    </div>
</div>
<c:if test="${cargo.status == 'NEW'}">
    <div class="ok">
        <button onclick="showForm()">Update</button>
    </div>
    <div class="ok">
        <button onclick="showDelete()">Delete</button>
    </div>
    <div class="ok">
        <button form="send" type="submit">Send</button>
    </div>
</c:if>
<div class="cancel">
    <button form="cancel" type="submit">Cancel</button>
</div>
<form id="cancel" method="get" action="/logistician/cargoes">
</form>
<form:form id="send" method="post" action="/logistician/cargoes/${cargo.number}/send">
</form:form>