<div class="table">
    <div class="row">
        <div>Number:</div>
        <div>${truck.number}</div>
    </div>
    <div class="row">
        <div>Shift size:</div>
        <div>${truck.shiftSize}</div>
    </div>
    <div class="row">
        <div>Capacity:</div>
        <div>${truck.capacity} tons</div>
    </div>
    <c:choose>
        <c:when test="${truck.valid == false}">
            <div class="row error">
                <div>Status:</div>
                <div>${truck.status}</div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="row">
                <div>Status:</div>
                <div>${truck.status}</div>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="row">
        <div>Location:</div>
        <div>${truck.location}</div>
    </div>
</div>
<div class="ok">
    <c:choose>
        <c:when test="${truck.number == null || truck.valid == false}">
            <button onclick="showForm()" disabled>Update</button>
        </c:when>
        <c:otherwise>
            <button onclick="showForm()">Update</button>
        </c:otherwise>
    </c:choose>
</div>
<div class="ok">
    <c:choose>
        <c:when test="${(truck.status == 'REMOVED' || truck.status == 'LOST')
        &&(truck.valid)}">
            <button onclick="showDelete()">Delete</button>
        </c:when>
        <c:otherwise>
            <button onclick="showDelete()" disabled>Delete</button>
        </c:otherwise>
    </c:choose>
</div>
<div class="cancel">
    <button form="cancel" type="submit">Cancel</button>
</div>
<form id="cancel" method="get" action="/logistician/trucks">
</form>