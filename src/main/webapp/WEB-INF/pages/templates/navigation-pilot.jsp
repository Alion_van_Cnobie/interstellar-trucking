<div class="left">
    <c:choose>
        <c:when test="${pilotInfo.currentPilot.status == 'ASSIGNED'}">
            <div>
                <button form="getWork" type="submit">Get work</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'FREE' || pilotInfo.currentPilot.status == 'DAY_OFF'}">
            <div>
                <button form="getWork" type="submit" disabled>Get work</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.isComplete == true}">
            <div>
                <button form="finishWork" type="submit">Finish work</button>
            </div>
        </c:when>
    </c:choose>
    <c:choose>
        <c:when test="${pilotInfo.currentPilot.status == 'DAY_OFF'}">
            <div>
                <button form="ready" type="submit">Ready</button>
            </div>
            <div>
                <button form="day-off" type="submit" disabled>Day off</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'FREE'}">
            <div>
                <button form="ready" type="submit" disabled>Ready</button>
            </div>
            <div>
                <button form="day-off" type="submit">Day off</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'REST' && pilotInfo.current.type ne 'TRANSIT' && pilotInfo.isLoading == true}">
            <div>
                <button form="takeControl" type="submit">Take control</button>
            </div>
            <div>
                <button form="startLoad" type="submit">Start loading</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'REST' && pilotInfo.isComplete == true}">
            <div>
                <button form="takeControl" type="submit" disabled>Take control</button>
            </div>
            <div>
                <button form="startLoad" type="submit" disabled>Start loading</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'REST'}">
            <div>
                <button form="takeControl" type="submit">Take control</button>
            </div>
            <div>
                <button form="startLoad" type="submit" disabled>Start loading</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'LOADING'}">
            <div>
                <button form="takeControl" type="submit" disabled>Take control</button>
            </div>
            <div>
                <button form="finishLoad" type="submit">Finish loading</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'PILOT' && (pilotInfo.isLoading == true || pilotInfo.isComplete == true)}">
            <div>
                <button form="hyperdrive" type="submit" disabled>Hyperdrive</button>
            </div>
            <div>
                <button form="broken" type="submit">Set broken</button>
            </div>
        </c:when>
        <c:when test="${pilotInfo.currentPilot.status == 'PILOT' && pilotInfo.isLoading == false}">
            <div>
                <button form="hyperdrive" type="submit">Hyperdrive</button>
            </div>
            <div>
                <button form="broken" type="submit">Set broken</button>
            </div>
        </c:when>
    </c:choose>
    <div>
        <button type="submit" class="logout" form="logoutForm">Logout</button>
    </div>
    <form:form id="logoutForm" method="post" action="/logout"></form:form>
    <form:form id="getWork" method="post" action="/pilot/start"></form:form>
    <form:form id="ready" method="post" action="/pilot/ready"></form:form>
    <form:form id="day-off" method="post" action="/pilot/dayoff"></form:form>
    <form:form id="takeControl" method="post" action="/pilot/control"></form:form>
    <form:form id="startLoad" method="post" action="/pilot/startload"></form:form>
    <form:form id="finishLoad" method="post" action="/pilot/finishload"></form:form>
    <form:form id="hyperdrive" method="post" action="/pilot/hyperdrive"></form:form>
    <form:form id="broken" method="post" action="/pilot/broken"></form:form>
    <form:form id="finishWork" method="post" action="/pilot/finishwork"></form:form>
</div>