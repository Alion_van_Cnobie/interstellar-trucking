<form:form id="new" method="post" action="/logistician/cargoes" modelAttribute="cargo">
    <div>
        <form:errors path="name" cssClass="error"/>
        <form:input  path="name" autocomplete="off" type="text" placeholder="Name"/>
    </div>
    <div>
        <form:errors path="weight" cssClass="error"/>
        <form:input  path="weight" autocomplete="off" type="number" placeholder="Weight"/>
    </div>
    <div>
        <label>Load location:</label>
        <form:errors path="start" cssClass="error"/>
        <form:select path="start">
            <c:forEach var="location" items="${locations}">
                <option value="${location.name}">${location.name}</option>
            </c:forEach>
        </form:select>
    </div>
    <div>
        <label>Unload location:</label>
        <form:errors path="end" cssClass="error"/>
        <form:select path="end">
            <c:forEach var="location" items="${locations}">
                <option value="${location.name}">${location.name}</option>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button type="submit" form="new">Submit</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>