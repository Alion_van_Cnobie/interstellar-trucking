<form:form id="new" method="post" action="/logistician/trucks" modelAttribute="truck">
    <div>
        <form:errors path="number" cssClass="error"/>
        <form:input  path="number" autocomplete="off" type="text" placeholder="Number"/>
    </div>
    <div>
        <form:errors path="shiftSize" cssClass="error"/>
        <form:input  path="shiftSize" autocomplete="off" type="number" placeholder="Shift size"/>
    </div>
    <div>
        <form:errors path="capacity" cssClass="error"/>
        <form:input  path="capacity" autocomplete="off" type="number" placeholder="Capacity"/>
    </div>
    <div>
        <form:select path="location">
            <c:forEach var="location" items="${locations}">
                <option value="${location.name}">${location.name}</option>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button type="submit" form="new">Submit</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>