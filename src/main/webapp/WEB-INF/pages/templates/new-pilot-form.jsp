<form:form id="new" method="post" action="/logistician/pilots" modelAttribute="pilot">
    <div>
        <form:errors path="firstName" cssClass="error"/>
        <form:input  path="firstName" autocomplete="off" type="text" placeholder="First name"/>
    </div>
    <div>
        <form:errors path="lastName" cssClass="error"/>
        <form:input path="lastName" autocomplete="off" type="text" name="last name" placeholder="Last name"/>
    </div>
    <div>
        <form:select path="location">
            <c:forEach var="location" items="${locations}">
                <option value="${location.name}">${location.name}</option>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button type="submit" form="new">Submit</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>