<form:form id="update" method="post" action="/logistician/cargoes/${cargo.number}/update" modelAttribute="cargoUpd">
    <div>
        <form:errors path="name" cssClass="error"/>
        <form:input  path="name" autocomplete="off" type="text" value="${cargo.name}"/>
    </div>
    <div>
        <form:errors path="weight" cssClass="error"/>
        <form:input  path="weight" autocomplete="off" type="number" value="${cargo.weight}"/>
    </div>
    <div>
        <label>Load location:</label>
        <form:errors path="start" cssClass="error"/>
        <form:select path="start">
            <c:forEach var="location" items="${locations}">
                <c:choose>
                    <c:when test="${location.name == cargo.start}">
                        <option selected value="${location.name}">${location.name}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${location.name}">${location.name}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
    <div>
        <label>Unload location:</label>
        <form:errors path="end" cssClass="error"/>
        <form:select path="end">
            <c:forEach var="location" items="${locations}">
                <c:choose>
                    <c:when test="${location.name == cargo.end}">
                        <option selected value="${location.name}">${location.name}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${location.name}">${location.name}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button form="update" type="submit">Update</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>