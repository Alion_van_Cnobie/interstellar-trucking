<div id="table">
    <div class="row" id="header-row">
        <div>Number</div>
        <div>Name</div>
        <div>Weight</div>
        <div>Load</div>
        <div>Unload</div>
        <div>Status</div>
    </div>
    <c:forEach var="cargo" items="${cargoList}">
        <a href="/logistician/cargoes/${cargo.number}" class="row">
            <div>${cargo.number}</div>
            <div>${cargo.name}</div>
            <div>${cargo.weight} tons</div>
            <div>${cargo.start}</div>
            <div>${cargo.end}</div>
            <div>${cargo.status}</div>
        </a>
    </c:forEach>
</div>
<div class="ok">
    <button onclick="showForm()">New cargo</button>
</div>
