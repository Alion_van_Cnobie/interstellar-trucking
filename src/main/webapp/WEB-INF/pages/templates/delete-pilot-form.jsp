<div id="table">
    <div class="row">
        <div>First name:</div>
        <div>${pilot.firstName}</div>
    </div>
    <div class="row">
        <div>Last name:</div>
        <div>${pilot.lastName}</div>
    </div>
    <div class="row">
        <div>Location:</div>
        <div>${pilot.location}</div>
    </div>
    <div class="row">
        <div>Private number:</div>
        <div>${pilot.number}</div>
    </div>
    <div class="row">
        <div>Status:</div>
        <div>${pilot.status}</div>
    </div>
    <div class="row">
        <div>Truck number:</div>
        <div>${pilot.truck}</div>
    </div>
    <div class="row">
        <div>Worked out:</div>
        <div>${pilot.workedOut}</div>
    </div>
</div>
<h3>Are you sure?</h3>
<div class="ok">
    <button form="deleteForm" type="submit">Yes</button>
</div>
<div class="cancel">
    <button onclick="hideDelete()">No</button>
</div>
<form:form id="deleteForm" method="post" action="/logistician/pilots/${pilot.number}/delete">
</form:form>