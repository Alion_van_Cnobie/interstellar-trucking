<div class="table">
    <div class="row">
        <div>First name:</div>
        <div>${pilot.firstName}</div>
    </div>
    <div class="row">
        <div>Last name:</div>
        <div>${pilot.lastName}</div>
    </div>
    <div class="row">
        <div>Location:</div>
        <div>${pilot.location}</div>
    </div>
    <div class="row">
        <div>Private number:</div>
        <div>${pilot.number}</div>
    </div>
    <c:choose>
        <c:when test="${pilot.valid == false}">
            <div class="row error">
                <div>Status:</div>
                <div>${pilot.status}</div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="row">
                <div>Status:</div>
                <div>${pilot.status}</div>
            </div>
        </c:otherwise>
    </c:choose>
    <div class="row">
        <div>Truck number:</div>
        <div>${pilot.truck}</div>
    </div>
    <div class="row">
        <div>Worked out:</div>
        <div>${pilot.workedOut} hours</div>
    </div>
</div>
<div class="ok">
    <c:choose>
        <c:when test="${pilot.number == null || pilot.valid == false}">
            <button onclick="showForm()" disabled>Update</button>
        </c:when>
        <c:otherwise>
            <button onclick="showForm()">Update</button>
        </c:otherwise>
    </c:choose>
</div>
<div class="ok">
    <c:choose>
        <c:when test="${(pilot.status == 'NEW' || pilot.status == 'FIRED' || pilot.status == 'LOST')
        &&(pilot.valid)}">
            <button onclick="showDelete()">Delete</button>
        </c:when>
        <c:otherwise>
            <button onclick="showDelete()" disabled>Delete</button>
        </c:otherwise>
    </c:choose>
</div>
<div class="cancel">
    <button form="cancel" type="submit">Cancel</button>
</div>
<form id="cancel" method="get" action="/logistician/pilots">
</form>