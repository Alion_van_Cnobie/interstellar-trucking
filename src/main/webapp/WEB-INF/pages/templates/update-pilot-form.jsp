<form:form id="update" method="post" action="/logistician/pilots/${pilot.number}/update" modelAttribute="pilotUpd">
    <div>
        <form:errors path="number" cssClass="error"/>
    </div>
    <div>
        <form:errors path="firstName" cssClass="error"/>
        <form:input  path="firstName" autocomplete="off" type="text" value="${pilot.firstName}"/>
    </div>
    <div>
        <form:errors path="lastName" cssClass="error"/>
        <form:input path="lastName" autocomplete="off" type="text" value="${pilot.lastName}"/>
    </div>
    <div>
        <form:errors path="workedOut" cssClass="error"/>
        <form:input path="workedOut" autocomplete="off" type="number" value="${pilot.workedOut}"/>
    </div>
    <div>
        <form:select path="status">
            <c:forEach var="status" items="${statuses}">
                <c:choose>
                    <c:when test="${pilot.status == status}">
                        <option value="${status}" selected>${status}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${status}">${status}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
    <div>
        <form:select path="location">
            <c:forEach var="location" items="${locations}">
                <c:choose>
                    <c:when test="${pilot.location == location}">
                        <option value="${location}" selected>${location}</option>
                    </c:when>
                    <c:otherwise>
                        <option value="${location}">${location}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </form:select>
    </div>
</form:form>
<div class="ok">
    <button form="update" type="submit">Update</button>
</div>
<div class="cancel">
    <button onclick="hideForm()">Cancel</button>
</div>