function hideForm() {
    document.getElementById("panel").classList.add("hidden");
    document.getElementById("show").classList.remove("hidden");
}

function showForm() {
    document.getElementById("show").classList.add("hidden");
    document.getElementById("panel").classList.remove("hidden");
}

function showDelete() {
    document.getElementById("show").classList.add("hidden");
    document.getElementById("delete").classList.remove("hidden");
}

function hideDelete() {
    document.getElementById("delete").classList.add("hidden");
    document.getElementById("show").classList.remove("hidden");
}
