package interstellarTrucking.model.DTO;


import interstellarTrucking.model.entities.Truck;
import org.springframework.util.StringUtils;

public class TruckDTO {
    private String number;
    private Integer shiftSize;
    private Integer capacity;
    private String status;
    private Integer locationId;
    private String location;
    private Boolean valid;
    private Integer distance;

    public TruckDTO() {
    }

    public TruckDTO(Truck truck) {
        this.number = truck.getNumber();
        this.shiftSize = truck.getShiftSize();
        this.capacity = truck.getCapacity();
        this.status = truck.getStatus().toString();
        this.locationId = truck.getLocation().getId();
        this.location = truck.getLocation().getName();
        this.valid = truck.getValid();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = StringUtils.trimAllWhitespace(number.toUpperCase());
    }

    public Integer getShiftSize() {
        return shiftSize;
    }

    public void setShiftSize(Integer shiftSize) {
        this.shiftSize = shiftSize;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
