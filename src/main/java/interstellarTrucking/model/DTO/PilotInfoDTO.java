package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Checkpoint;
import interstellarTrucking.model.entities.Pilot;

import java.util.ArrayList;
import java.util.List;

public class PilotInfoDTO {

    private PilotDTO currentPilot;
    private String orderNumber;
    private Checkpoint current;
    private List<PilotDTO> copilots = new ArrayList<>();
    private List<Checkpoint> checkpoints = new ArrayList<>();
    private boolean isLoading;
    private boolean isComplete;

    public PilotInfoDTO(Pilot pilot) {
        currentPilot = new PilotDTO(pilot);
    }

    public PilotDTO getCurrentPilot() {
        return currentPilot;
    }

    public void setCurrentPilot(PilotDTO currentPilot) {
        this.currentPilot = currentPilot;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<PilotDTO> getCopilots() {
        return copilots;
    }

    public void setCopilots(List<PilotDTO> copilots) {
        this.copilots = copilots;
    }

    public List<Checkpoint> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Checkpoint> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public Checkpoint getCurrent() {
        return current;
    }

    public void setCurrent(Checkpoint current) {
        this.current = current;
    }

    public boolean getIsLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public boolean getIsComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
