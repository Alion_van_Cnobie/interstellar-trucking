package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Pilot;
import org.springframework.util.StringUtils;

import java.io.Serializable;

public class PilotDTO implements Serializable {

    private String firstName;
    private String lastName;
    private String number;
    private String status;
    private Integer workedOut;
    private String location;
    private Integer user;
    private Boolean valid;
    private String truck;

    public PilotDTO() {
    }

    public PilotDTO(Pilot pilot) {

        this.firstName = pilot.getFirstName();
        this.lastName = pilot.getLastName();
        this.number = pilot.getPrivateNumber();
        this.status = pilot.getStatus().toString();
        this.location = pilot.getLocation().getName();
        this.user = pilot.getUser().getId();
        this.valid = pilot.getValid();
        this.workedOut = pilot.getWorkedOut()/60;
        if (pilot.getTruck() == null) {
            this.truck = "";
        } else {
            this.truck = pilot.getTruck().getNumber();
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public String getStatus() {
        return status;
    }

    public String getLocation() {
        return location;
    }

    public Integer getUser() {
        return user;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setFirstName(String firstName) {
        this.firstName = StringUtils.capitalize(firstName.trim().toLowerCase());
    }

    public void setLastName(String lastName) {
        this.lastName = StringUtils.capitalize(lastName.trim().toLowerCase());
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Integer getWorkedOut() {
        return workedOut;
    }

    public void setWorkedOut(Integer workedOut) {
        this.workedOut = workedOut;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }
}
