package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Order;
import interstellarTrucking.model.service.interfaces.CheckpointService;
import org.springframework.beans.factory.annotation.Autowired;


public class OrderDTO {

    private String start;
    private String finish;
    private String current;
    private String number;
    private String status;
    private String truck;
    private Integer notLoaded;
    private Integer loaded;
    private Integer delivered;
    private Integer estimatedTime;
    private Integer maxLoad;
    private Integer currentPosition;

    public OrderDTO() {
    }

    public OrderDTO(Order order) {
        this.number = order.getUniqueNumber();
        this.status = order.getStatus().toString();
        if (order.getTruck() != null) {
            this.truck = order.getTruck().getNumber();
        } else {
            this.truck = "";
        }
        this.estimatedTime = order.getEstimatedTime();
        this.start = order.getStart().getName();
        this.finish = order.getFinish().getName();
        this.maxLoad = order.getMaxLoad();
        this.currentPosition = order.getCurrentPosition();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public Integer getMaxLoad() {
        return maxLoad;
    }

    public void setMaxLoad(Integer maxLoad) {
        this.maxLoad = maxLoad;
    }

    public String getCurrent() {
        return current;
    }

    public void setCurrent(String current) {
        this.current = current;
    }

    public Integer getNotLoaded() {
        return notLoaded;
    }

    public void setNotLoaded(Integer notLoaded) {
        this.notLoaded = notLoaded;
    }

    public Integer getLoaded() {
        return loaded;
    }

    public void setLoaded(Integer loaded) {
        this.loaded = loaded;
    }

    public Integer getDelivered() {
        return delivered;
    }

    public void setDelivered(Integer delivered) {
        this.delivered = delivered;
    }

    public Integer getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Integer currentPosition) {
        this.currentPosition = currentPosition;
    }
}
