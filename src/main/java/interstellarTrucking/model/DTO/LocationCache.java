package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Distance;
import interstellarTrucking.model.entities.Hyperway;
import interstellarTrucking.model.entities.Location;

import java.util.HashMap;
import java.util.Map;

public class LocationCache {

    private Map<Location, Integer> hyperways = new HashMap<>();
    private Map<Location, DistanceCache> distances = new HashMap<>();

    public Map<Location, Integer> getHyperways() {
        return hyperways;
    }

    public void setHyperways(Map<Location, Integer> hyperways) {
        this.hyperways = hyperways;
    }

    public Map<Location, DistanceCache> getDistances() {
        return distances;
    }

    public void setDistances(Map<Location, DistanceCache> distances) {
        this.distances = distances;
    }

    public Integer getHyperwayLength(Location location) {
        return hyperways.get(location);
    }

    public Integer getDistance(Location location) {
        return distances.get(location).getDistance();
    }

    public void addHyperway(Hyperway hyperway) {
        hyperways.put(hyperway.getFinish(), hyperway.getDistance());
    }

    public void addDistance(Distance distance) {
        distances.put(distance.getFinish(), new DistanceCache(distance.getDistance(), distance.getBypassNumber(), distance.getPrev()));
    }
}
