package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Location;

public class DistanceCache {

    private Integer distance;
    private Integer number;
    private Location prev;

    public DistanceCache(Integer distance, Integer number, Location prev) {
        this.distance = distance;
        this.number = number;
        this.prev = prev;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Location getPrev() {
        return prev;
    }

    public void setPrev(Location prev) {
        this.prev = prev;
    }
}
