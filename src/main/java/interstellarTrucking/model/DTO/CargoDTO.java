package interstellarTrucking.model.DTO;

import interstellarTrucking.model.entities.Cargo;

public class CargoDTO {

    private String number;
    private String name;
    private Integer weight;
    private String start;
    private String end;
    private String status;

    public CargoDTO() {
    }

    public CargoDTO(Cargo cargo) {
        this.number = cargo.getNumber();
        this.name = cargo.getName();
        this.weight = cargo.getWeight();
        this.start = cargo.getStart().getName();
        this.end = cargo.getEnd().getName();
        this.status = cargo.getStatus().toString();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
