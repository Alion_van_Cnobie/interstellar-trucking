package interstellarTrucking.model.DTO;

import java.util.List;

public class BoardDTO{

    private List<OrderDTO> orders;
    private Integer availablePilots;
    private Integer unavailablePilots;
    private Integer freeTrucks;
    private Integer busyTrucks;
    private Integer brokenTrucks;

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }

    public Integer getAvailablePilots() {
        return availablePilots;
    }

    public void setAvailablePilots(Integer availablePilots) {
        this.availablePilots = availablePilots;
    }

    public Integer getUnavailablePilots() {
        return unavailablePilots;
    }

    public void setUnavailablePilots(Integer unavailablePilots) {
        this.unavailablePilots = unavailablePilots;
    }

    public Integer getFreeTrucks() {
        return freeTrucks;
    }

    public void setFreeTrucks(Integer freeTrucks) {
        this.freeTrucks = freeTrucks;
    }

    public Integer getBusyTrucks() {
        return busyTrucks;
    }

    public void setBusyTrucks(Integer busyTrucks) {
        this.busyTrucks = busyTrucks;
    }

    public Integer getBrokenTrucks() {
        return brokenTrucks;
    }

    public void setBrokenTrucks(Integer brokenTrucks) {
        this.brokenTrucks = brokenTrucks;
    }

}
