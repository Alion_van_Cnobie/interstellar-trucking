package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.PilotDTO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Pilot;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.User;
import interstellarTrucking.model.entities.enums.PilotStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PilotService {

    String add(PilotDTO pilotDTO);

    void delete(String number);

    void update(String number, PilotDTO pilotDTO);

    void update(Pilot pilot);

    List<Pilot> getAll();

    List<Pilot> getAllValid();

    Pilot getById(Integer id);

    PilotDTO getByNumber(String number);

    List<Pilot> getAllFreeByLocation(Location location);

    Pilot getByUser(User user);

    Pilot getCurrentPilot();

    List<Pilot> getAllValidWithStatus(PilotStatus status);

    List<Pilot> getAllValidWithoutStatus(PilotStatus status);

    void setStatus(Pilot pilot, PilotStatus status);

    void setLocation(Pilot pilot, Location location);

    void setTruck(Pilot pilot, Truck truck);

    List<PilotDTO> convertEntityListToDTO(List<Pilot> pilots);

}
