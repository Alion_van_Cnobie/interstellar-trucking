package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.CheckpointType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CheckpointService {
    List<Checkpoint> getAllByOrder(Order order);

    Checkpoint getByOrderAndPosition(Order order, Integer position);

    void delete(Checkpoint checkpoint);

    void deleteTransitCheckpoints(Order order);

    void add(List<Checkpoint> checkpoints, Order order);

    boolean add(Order order, Cargo cargo);

    void checkCheckpoints(List<Checkpoint> checkpoints);

    void addTruckToOrder(Truck truck, Order order);

    Checkpoint createCheckpoint(Cargo cargo, CheckpointType type);
}
