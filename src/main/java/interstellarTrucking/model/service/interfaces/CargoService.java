package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.CargoDTO;
import interstellarTrucking.model.entities.Cargo;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.enums.CargoStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CargoService {
    Cargo getById(Integer id);

    CargoDTO getByNumber(String number);

    String add(CargoDTO cargoDTO);

    void update(CargoDTO cargoDTO);

    void setStatus(Cargo cargo, CargoStatus cargoStatus);

    void delete(String number);

    void send(String number);

    void emergencyUnload(Cargo cargo, Location location);

    List<Cargo> getAllProcessing();

    List<Cargo> getAllWithStatus(CargoStatus status);

    List<CargoDTO> convertEntityListToDTO(List<Cargo> cargoes);
}
