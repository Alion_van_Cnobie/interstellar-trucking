package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.TruckDTO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.TruckStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TruckService {

    boolean add(TruckDTO truckDTO);

    void delete(String number);

    void update(String number, TruckDTO truckDTO);

    void update(Truck truck);

    List<Truck> getAll();

    List<Truck> getAllValid();

    Truck getById(Integer id);

    TruckDTO getByNumber(String number);

    List<Truck> getAllValidWithStatus(TruckStatus status);

    List<Truck> getAllReadyWithCapacity(Integer capacity);

    List<TruckDTO> convertEntityListToDTO(List<Truck> trucks);

    void setLocation(Truck truck, Location location);

    void setStatus(Truck truck, TruckStatus status);
}
