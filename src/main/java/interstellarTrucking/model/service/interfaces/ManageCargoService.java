package interstellarTrucking.model.service.interfaces;

import org.springframework.stereotype.Service;

@Service
public interface ManageCargoService {
    void cacheLocations();

    void processCargoes();
}
