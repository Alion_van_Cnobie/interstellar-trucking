package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.PilotCredentials;
import interstellarTrucking.model.DTO.PilotInfoDTO;
import org.springframework.stereotype.Service;

@Service
public interface PilotControlService {
    PilotInfoDTO getCurrentPilotInfo();

    boolean updateCredentials(PilotCredentials credentials);

    void getWork();

    void setReady();

    void setDayoff();

    void takeControl();

    void startLoading();

    void finishLoading();

    void nextLocation();

    void setBroken();

    void finishWork();
}
