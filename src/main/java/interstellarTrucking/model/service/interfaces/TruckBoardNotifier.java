package interstellarTrucking.model.service.interfaces;

import org.springframework.stereotype.Service;

@Service
public interface TruckBoardNotifier {

    void sendNotice(final String notice);
}
