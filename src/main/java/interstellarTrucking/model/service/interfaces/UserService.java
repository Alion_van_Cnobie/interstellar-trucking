package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.entities.User;
import interstellarTrucking.model.entities.enums.UserRole;

public interface UserService {

    User getByUsername(String username);

    User getCurrentUser();

    User updateCurrentUser(String username, String password);

    void update(User user);

    void delete(User user, boolean full);

    User createUser(String username, String password, UserRole role);

}
