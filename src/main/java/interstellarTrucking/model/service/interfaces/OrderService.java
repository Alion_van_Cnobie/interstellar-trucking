package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.OrderDTO;
import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.OrderStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderService {

    void add(Order order, Cargo cargo);

    void create(Order order);

    Order getByPilot(Pilot pilot);

    List<Order> getAllWithoutStatus(OrderStatus status);

    void delete(Order order);

    void deleteCreated();

    void update(Order order);

    void update(List<Checkpoint> merged, Order orderTemplate);

    List<Order> getAll();

    Order getById(Integer id);

    Order getByNumber(String number);

    List<Order> getAllWithStatus(OrderStatus status);

    List<OrderDTO> convertEntityListToDTO(List<Order> orders);

    Order getByTruck(Truck truck);

    Order getLastByTruck(Truck truck);

    void complete(Order order);

    void setPosition(Order order, Integer position);

    void interrupt(Order order);

    OrderDTO convertEntityToDTO(Order order);
}
