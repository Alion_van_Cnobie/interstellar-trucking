package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.entities.Location;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LocationService {
    List<Location> getAll();

    Location getById(Integer id);
}
