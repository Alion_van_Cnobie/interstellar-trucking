package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.entities.Location;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DistanceService {
    List<Location> getRoute(Location start, Location end);

    Integer getDistance(Location start, Location end);

    void cacheHyperMap();

    void calculateHyperMap();

    void writeHyperMap();
}
