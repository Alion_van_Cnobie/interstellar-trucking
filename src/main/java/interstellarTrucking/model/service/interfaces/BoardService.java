package interstellarTrucking.model.service.interfaces;

import interstellarTrucking.model.DTO.BoardDTO;
import org.springframework.stereotype.Service;

@Service
public interface BoardService {
    BoardDTO getBoardDTO();
}
