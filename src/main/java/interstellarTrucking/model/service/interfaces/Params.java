package interstellarTrucking.model.service.interfaces;

public interface Params {
    static final Integer MAX_WORKED_OUT = 176;
    static final Integer LOADING_DURATION = 1;
    static final  Integer AFFORDABLE_DELAY = 10;
}
