package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.CargoDAO;
import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.model.DTO.CargoDTO;
import interstellarTrucking.model.entities.Cargo;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.enums.CargoStatus;
import interstellarTrucking.model.service.interfaces.CargoService;
import interstellarTrucking.utils.PrivateNumberGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CargoServiceImpl implements CargoService {
    private final CargoDAO<Cargo> cargoDAO;
    private final LocationDAO<Location> locationDAO;

    public CargoServiceImpl(CargoDAO<Cargo> cargoDAO, LocationDAO<Location> locationDAO) {
        this.cargoDAO = cargoDAO;
        this.locationDAO = locationDAO;
    }

    @Override
    public Cargo getById(Integer id) {
        return cargoDAO.getById(id);
    }

    @Override
    public CargoDTO getByNumber(String number) {
        Cargo cargo = cargoDAO.getByNumber(number);
        return (cargo == null) ? null : new CargoDTO(cargo);
    }

    @Override
    public String add(CargoDTO cargoDTO) {
        checkDTO(cargoDTO, true);
        String number;
        do {
            number = PrivateNumberGenerator.generatePrivateNumber();
        } while (cargoDAO.getByNumber(number) != null);
        cargoDTO.setNumber(number);
        Cargo cargo = new Cargo();
        convertDTOtoEntity(cargoDTO, cargo);
        cargo.setStatus(CargoStatus.NEW);
        cargoDAO.add(cargo);
        return number;
    }

    @Override
    public void update(CargoDTO cargoDTO) {
        checkDTO(cargoDTO, false);
        Cargo cargo = cargoDAO.getByNumber(cargoDTO.getNumber());
        if (cargo == null) return;
        if (cargo.getStatus() != CargoStatus.NEW) return;
        convertDTOtoEntity(cargoDTO, cargo);
        cargoDAO.update(cargo);
    }

    @Override
    public void setStatus(Cargo cargo, CargoStatus cargoStatus) {
        cargo.setStatus(cargoStatus);
        cargoDAO.update(cargo);
    }

    @Override
    public void delete(String number) {
        Cargo cargo = cargoDAO.getByNumber(number);
        if (cargo == null || cargo.getStatus() != CargoStatus.NEW) return;
        cargoDAO.delete(cargo);
    }

    @Override
    public void send(String number) {
        Cargo cargo = cargoDAO.getByNumber(number);
        if (cargo == null || cargo.getStatus() != CargoStatus.NEW) return;
        cargo.setStatus(CargoStatus.READY);
        cargoDAO.update(cargo);
    }

    @Override
    public void emergencyUnload(Cargo cargo, Location location) {
        cargo.setStatus(CargoStatus.READY);
        cargo.setStart(location);
        cargoDAO.update(cargo);
    }

    @Override
    public List<Cargo> getAllProcessing()
    {
        return cargoDAO.getAllProcessing();
    }

    @Override
    public List<Cargo> getAllWithStatus(CargoStatus status) {
        return cargoDAO.getAllWithStatus(status);
    }

    @Override
    public List<CargoDTO> convertEntityListToDTO(List<Cargo> cargoes) {
        List<CargoDTO> cargoDTOList = new ArrayList<>();

        for (Cargo cargo: cargoes) {
            cargoDTOList.add(new CargoDTO(cargo));
        }

        return cargoDTOList;
    }

    private void convertDTOtoEntity(CargoDTO cargoDTO, Cargo cargo) {
        cargo.setNumber(cargoDTO.getNumber());
        cargo.setName(cargoDTO.getName());
        cargo.setWeight(cargoDTO.getWeight());
        Location start = locationDAO.getByName(cargoDTO.getStart());
        if (start == null ) throw new IllegalArgumentException("No such download location: " + cargoDTO.getStart());
        cargo.setStart(start);
        Location end = locationDAO.getByName(cargoDTO.getEnd());
        if (end == null ) throw new IllegalArgumentException("No such upload location: " + cargoDTO.getEnd());
        cargo.setEnd(end);
    }

    private void checkDTO(CargoDTO cargoDTO, boolean isNew) {
        String message = "";
        boolean exception = false;
        boolean locations = true;

        if (!isNew) {
            if (cargoDTO.getNumber() == null || cargoDTO.getNumber().isEmpty()) {
                message += "No number! ";
                exception = true;
            }
        }

        if (cargoDTO.getName() == null || cargoDTO.getName().isEmpty()) {
            message += "No name! ";
            exception = true;
        }

        if (cargoDTO.getStart() == null || cargoDTO.getStart().isEmpty()) {
            message += "No download location! ";
            exception = true;
            locations = false;
        }

        if (cargoDTO.getEnd() == null || cargoDTO.getEnd().isEmpty()) {
            message += "No upload location! ";
            exception = true;
            locations = false;
        }

        if (locations && cargoDTO.getStart().equals(cargoDTO.getEnd())) {
            message += "Same locations!";
            exception = true;
        }

        if (exception) throw new IllegalArgumentException(message);
    }
}
