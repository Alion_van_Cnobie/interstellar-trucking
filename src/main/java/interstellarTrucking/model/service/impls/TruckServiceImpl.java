package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.DAO.interfaces.TruckDAO;
import interstellarTrucking.model.DTO.TruckDTO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.TruckStatus;
import interstellarTrucking.model.service.interfaces.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TruckServiceImpl implements TruckService {

    private final TruckDAO<Truck> truckDAO;
    private final LocationDAO<Location> locationDAO;

    @Autowired
    public TruckServiceImpl(TruckDAO<Truck> truckDAO, LocationDAO<Location> locationDAO) {
        this.truckDAO = truckDAO;
        this.locationDAO = locationDAO;
    }

    @Override
    public boolean add(TruckDTO truckDTO) {
        truckDTO.setStatus(TruckStatus.READY.toString());
        checkDTO(truckDTO);
        if (truckDAO.getByNumber(truckDTO.getNumber()) != null) return false;
        Truck truck = new Truck();
        convertDTOtoEntity(truckDTO, truck);
        truck.setValid(true);
        truckDAO.add(truck);
        return true;
    }

    @Override
    public void delete(String number) {
        Truck truck = truckDAO.getByNumber(number);
        if (truck == null || !truck.getValid()) return;
        if (truck.getStatus() != TruckStatus.LOST && truck.getStatus() != TruckStatus.REMOVED) return;
        truck.setValid(false);
        truckDAO.update(truck);
    }

    @Override
    public void update(String number, TruckDTO truckDTO) {
        checkDTO(truckDTO);
        Truck oldTruck = truckDAO.getByNumber(number);
        if (oldTruck == null) return;
        if (!oldTruck.getValid()) return;
        if (TruckStatus.valueOf(truckDTO.getStatus()) == TruckStatus.BUSY) return;
        convertDTOtoEntity(truckDTO, oldTruck);
        truckDAO.update(oldTruck);
    }

    @Override
    public void update(Truck truck) {
        truckDAO.update(truck);
    }

    @Override
    public List<Truck> getAll() {
        return truckDAO.getAll();
    }

    @Override
    public List<Truck> getAllValid() {
        return truckDAO.getAllValid();
    }

    @Override
    public Truck getById(Integer id) {
        return truckDAO.getById(id);
    }

    @Override
    public TruckDTO getByNumber(String number) {
        Truck truck = truckDAO.getByNumber(number);
        return (truck == null) ? null : new TruckDTO(truck);
    }

    @Override
    public List<Truck> getAllValidWithStatus(TruckStatus status) {
        return truckDAO.getAllValidWithStatus(status);
    }

    @Override
    public List<Truck> getAllReadyWithCapacity(Integer capacity) {
        return truckDAO.getAllReadyWithCapacity(capacity);
    }

    @Override
    public List<TruckDTO> convertEntityListToDTO(List<Truck> trucks) {
        List<TruckDTO> truckDTOList = new ArrayList<>();

        for (Truck truck : trucks) {
            truckDTOList.add(new TruckDTO(truck));
        }

        return truckDTOList;
    }

    @Override
    public void setLocation(Truck truck, Location location) {
        truck.setLocation(location);
        truckDAO.update(truck);
    }

    @Override
    public void setStatus(Truck truck, TruckStatus status) {
        truck.setStatus(status);
        truckDAO.update(truck);
    }

    private void convertDTOtoEntity(TruckDTO truckDTO, Truck truck) {
        truck.setNumber(truckDTO.getNumber());
        truck.setCapacity(truckDTO.getCapacity());
        truck.setShiftSize(truckDTO.getShiftSize());
        Location location = locationDAO.getByName(truckDTO.getLocation());
        if (location == null ) throw new IllegalArgumentException("No such location: " + truckDTO.getLocation());
        truck.setLocation(location);
        truck.setStatus(TruckStatus.valueOf(truckDTO.getStatus()));
    }

    private void checkDTO(TruckDTO truckDTO) {
        String message = "";
        boolean exception = false;

        if (truckDTO.getNumber() == null || truckDTO.getNumber().isEmpty()) {
            message += "No number! ";
            exception = true;
        }

        if (truckDTO.getShiftSize() == null || truckDTO.getShiftSize() < 1) {
            message += "No shift size! ";
            exception = true;
        }

        if (truckDTO.getCapacity() == null || truckDTO.getCapacity() < 1) {
            message += "No capacity! ";
            exception = true;
        }

        if (truckDTO.getStatus() == null || truckDTO.getStatus().isEmpty()) {
            message += "No status! ";
            exception = true;
        }

        if (truckDTO.getLocation() == null || truckDTO.getLocation().isEmpty()) {
            message += "No location!";
            exception = true;
        }

        if (exception) throw new IllegalArgumentException(message);
    }
}
