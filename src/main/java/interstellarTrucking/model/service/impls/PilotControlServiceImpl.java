package interstellarTrucking.model.service.impls;

import interstellarTrucking.model.DTO.PilotCredentials;
import interstellarTrucking.model.DTO.PilotInfoDTO;
import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.*;
import interstellarTrucking.model.service.interfaces.*;
import org.joda.time.MutableDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

@Service
@Transactional
public class PilotControlServiceImpl implements PilotControlService {

    private final PilotService pilotService;
    private final OrderService orderService;
    private final CheckpointService checkpointService;
    private final UserService userService;
    private final CargoService cargoService;
    private final TruckService truckService;

    @Autowired
    public PilotControlServiceImpl(PilotService pilotService, OrderService orderService,
                                   CheckpointService checkpointService, UserService userService,
                                   CargoService cargoService, TruckService truckService) {
        this.pilotService = pilotService;
        this.orderService = orderService;
        this.checkpointService = checkpointService;
        this.userService = userService;
        this.cargoService = cargoService;
        this.truckService = truckService;
    }

    @Override
    public PilotInfoDTO getCurrentPilotInfo() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() == PilotStatus.NEW) return null;
        Order order = orderService.getByPilot(pilot);
        PilotInfoDTO pilotInfoDTO = new PilotInfoDTO(pilot);
        if (order != null) {
            if (hasUnloadedCargoes(order)) pilotInfoDTO.setLoading(true);
            Set<Pilot> copilots = order.getPilots();
            pilotInfoDTO.setCopilots(pilotService.convertEntityListToDTO(new ArrayList<>(copilots)));
            pilotInfoDTO.setCheckpoints(checkpointService.getAllByOrder(order));
            pilotInfoDTO.setOrderNumber(order.getUniqueNumber());
            pilotInfoDTO.setCurrent(checkpointService.getByOrderAndPosition(order, order.getCurrentPosition()));
            if (order.getStatus() == OrderStatus.DONE || order.getStatus() == OrderStatus.INTERRUPTED) {
                pilotInfoDTO.setComplete(true);
            }
        }
        return pilotInfoDTO;
    }

    @Override
    public boolean updateCredentials(PilotCredentials credentials) {
        checkCredentials(credentials);
        Pilot pilot = pilotService.getCurrentPilot();
        User user = userService.updateCurrentUser(credentials.getUsername(), credentials.getPassword());
        if (user == null) return false;
        pilot.setUser(user);
        pilot.setStatus(PilotStatus.FREE);
        pilotService.update(pilot);
        return true;
    }

    @Override
    public void getWork() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.ASSIGNED) return;

        Set<Pilot> pilots = orderService.getByPilot(pilot).getPilots();

        for (Pilot copilot: pilots) {
            if (copilot.getId().equals(pilot.getId())) continue;
            if (copilot.getStatus() == PilotStatus.PILOT) {
                pilot.setStatus(PilotStatus.REST);
                pilotService.update(pilot);
                return;
            }
        }

        pilot.setStatus(PilotStatus.PILOT);
        pilot.setChangeStatusTime(new Date());
        pilotService.update(pilot);
    }

    @Override
    public void setReady() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.DAY_OFF) return;
        pilot.setStatus(PilotStatus.FREE);
        pilotService.update(pilot);
    }

    @Override
    public void setDayoff() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.FREE) return;
        pilot.setStatus(PilotStatus.DAY_OFF);
        pilotService.update(pilot);
    }

    @Override
    public void takeControl() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.REST) return;
        Set<Pilot> pilots = orderService.getByPilot(pilot).getPilots();
        for (Pilot copilot: pilots) {
            if (copilot.getStatus() == PilotStatus.PILOT) {
                addWorkTime(copilot);
                pilotService.setStatus(copilot, PilotStatus.REST);
            }
        }

        pilot.setChangeStatusTime(new Date());
        pilotService.setStatus(pilot, PilotStatus.PILOT);
    }

    @Override
    public void startLoading() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.REST) return;
        Order order = orderService.getByPilot(pilot);
        if (!hasUnloadedCargoes(order)) return;
        pilot.setChangeStatusTime(new Date());
        pilotService.setStatus(pilot, PilotStatus.LOADING);
    }

    @Override
    public void finishLoading() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.LOADING) return;
        addWorkTime(pilot);
        pilotService.setStatus(pilot, PilotStatus.REST);

        Order order = orderService.getByPilot(pilot);
        Set<Pilot> copilots = order.getPilots();

        for(Pilot copilot : copilots) {
            if (copilot.getId().equals(pilot.getId())) continue;
            if (copilot.getStatus() == PilotStatus.LOADING) return;
        }

        loading(order);
    }

    @Override
    public void nextLocation() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.PILOT) return;
        Order order = orderService.getByPilot(pilot);
        if (hasUnloadedCargoes(order)) return;
        move(order);
    }

    @Override
    public void setBroken() {
        Pilot pilot = pilotService.getCurrentPilot();
        if (pilot.getStatus() != PilotStatus.PILOT) return;
        orderService.interrupt(orderService.getByPilot(pilot));
    }

    @Override
    public void finishWork() {
        Pilot pilot = pilotService.getCurrentPilot();
        Order order = orderService.getByPilot(pilot);
        Truck truck = pilot.getTruck();
        if (order.getStatus() != OrderStatus.DONE && order.getStatus() != OrderStatus.INTERRUPTED) return;
        if (pilot.getStatus() == PilotStatus.PILOT || pilot.getStatus() == PilotStatus.LOADING) {
            addWorkTime(pilot);
        }

        pilot.setTruck(null);
        pilotService.setStatus(pilot, PilotStatus.FREE);

        for (Pilot copilot: order.getPilots()) {
            if (copilot.getId().equals(pilot.getId())) continue;
            if (copilot.getTruck() != null) return;
        }

        if (truck.getStatus() != TruckStatus.BROKEN) {
            truckService.setStatus(truck, TruckStatus.READY);
        }
    }

    private void move(Order order) {
        if (order.getStatus() != OrderStatus.PROCESSING) return;
        Set<Pilot> pilots = order.getPilots();
        for (Pilot pilot : pilots) {
            if (pilot.getStatus() != PilotStatus.REST && pilot.getStatus() != PilotStatus.PILOT) {
                return;
            }
        }

        Integer position = order.getCurrentPosition();
        Checkpoint current = checkpointService.getByOrderAndPosition(order, position);
        Checkpoint checkpoint = current;

        while (checkpoint.getLocation().getId().equals(current.getLocation().getId())) {
            checkpoint = checkpointService.getByOrderAndPosition(order, ++position);
            if (checkpoint == null) return;
        }

        orderService.setPosition(order, checkpoint.getPosition());
        truckService.setLocation(order.getTruck(), checkpoint.getLocation());

        for (Pilot pilot : pilots) {
            pilotService.setLocation(pilot, checkpoint.getLocation());
        }
    }

    private void loading(Order order) {
        Integer position = order.getCurrentPosition();
        Checkpoint current = checkpointService.getByOrderAndPosition(order, position);
        Checkpoint checkpoint = current;

        while (checkpoint.getLocation().getId().equals(current.getLocation().getId())) {
            if (checkpoint.getType() == CheckpointType.LOAD) {
                cargoService.setStatus(checkpoint.getCargo(), CargoStatus.LOADED);
            }

            if (checkpoint.getType() == CheckpointType.UNLOAD) {
                cargoService.setStatus(checkpoint.getCargo(), CargoStatus.DELIVERED);
            }

            checkpoint = checkpointService.getByOrderAndPosition(order, ++position);

            if (checkpoint == null) {
                orderService.complete(order);
                return;
            }
        }
    }

    private boolean hasUnloadedCargoes(Order order) {
        Integer position = order.getCurrentPosition();
        Checkpoint current = checkpointService.getByOrderAndPosition(order, position);
        Checkpoint checkpoint = current;
        if (checkpoint.getType() == CheckpointType.TRANSIT) return false;

        while (checkpoint.getLocation().getId().equals(current.getLocation().getId())) {
            if (checkpoint.getType() == CheckpointType.LOAD
                    && checkpoint.getCargo().getStatus() != CargoStatus.LOADED) return true;
            if (checkpoint.getType() == CheckpointType.UNLOAD
                    && checkpoint.getCargo().getStatus() != CargoStatus.DELIVERED) return true;

            checkpoint = checkpointService.getByOrderAndPosition(order, ++position);

            if (checkpoint == null) {
                return false;
            }
        }

        return false;
    }

    private void addWorkTime(Pilot pilot) {
        MutableDateTime changeStatusTime = new MutableDateTime(pilot.getChangeStatusTime());
        MutableDateTime currentTime = new MutableDateTime();

        if (currentTime.getMonthOfYear() != changeStatusTime.getMonthOfYear()) {
            changeStatusTime.addMonths(1);
            changeStatusTime.setDayOfMonth(1);
            changeStatusTime.setMillisOfDay(0);
            pilot.setWorkedOut(0);
        }

        pilot.setChangeStatusTime(new Date());
        pilot.setWorkedOut(pilot.getWorkedOut() + (int)(currentTime.getMillis() - changeStatusTime.getMillis())/60000);
    }

    private void checkCredentials(PilotCredentials credentials) {
        boolean exception = false;
        String message = "";

        if (credentials.getUsername() == null) {
            exception = true;
            message += "No username! ";
        }

        if (credentials.getPassword() == null) {
            exception = true;
            message += "No password! ";
        }

        if (credentials.getConfirm() == null) {
            exception = true;
            message += "No password confirmation! ";
        }

        if (!exception && !credentials.getPassword().equals(credentials.getConfirm())) {
            exception = true;
            message += "Password and confirmation do not match!";
        }

        if (exception) throw new IllegalArgumentException(message);
    }
}
