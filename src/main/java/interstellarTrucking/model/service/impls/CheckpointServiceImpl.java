package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.CargoDAO;
import interstellarTrucking.DAO.interfaces.CheckpointDAO;
import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.CargoStatus;
import interstellarTrucking.model.entities.enums.CheckpointType;
import interstellarTrucking.model.service.interfaces.CheckpointService;
import interstellarTrucking.model.service.interfaces.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CheckpointServiceImpl implements CheckpointService {

    private final CheckpointDAO<Checkpoint> checkpointDAO;
    private final CargoDAO<Cargo> cargoDAO;
    private final DistanceService distanceService;

    @Autowired
    public CheckpointServiceImpl(CheckpointDAO<Checkpoint> checkpointDAO, CargoDAO<Cargo> cargoDAO, DistanceService distanceService) {
        this.checkpointDAO = checkpointDAO;
        this.cargoDAO = cargoDAO;
        this.distanceService = distanceService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Checkpoint> getAllByOrder(Order order) {
        return checkpointDAO.getAllByOrder(order);
    }

    @Override
    @Transactional(readOnly = true)
    public Checkpoint getByOrderAndPosition(Order order, Integer position) {
        return checkpointDAO.getByOrderAndPosition(order, position);
    }

    @Override
    @Transactional
    public void delete(Checkpoint checkpoint) {
        checkpointDAO.delete(checkpoint);
    }

    @Override
    @Transactional
    public void deleteTransitCheckpoints(Order order) {
        boolean firstCargoLoaded = false;
        for (Checkpoint checkpoint: getAllByOrder(order)) {
            if (checkpoint.getType() == CheckpointType.LOAD) firstCargoLoaded = true;
            if (!firstCargoLoaded) continue;
            if (checkpoint.getType() == CheckpointType.TRANSIT) {
                checkpointDAO.delete(checkpoint);
            }
        }
    }

    @Override
    @Transactional
    public void add(List<Checkpoint> checkpoints, Order order) {
        List<Checkpoint> routeList = new ArrayList<>();

        for (int i = 0; i < checkpoints.size()-1; i++) {
            Checkpoint current = checkpoints.get(i);
            if (i == 0) {
                routeList.add(current);
            }

            Checkpoint next = checkpoints.get(i+1);
            if (current.getLocation().getId().equals(next.getLocation().getId())) {
                routeList.add(next);
                continue;
            }

            List<Location> route = distanceService.getRoute(current.getLocation(), next.getLocation());
            routeList.addAll(getTransitCheckpoints(route));
            routeList.add(next);
        }

        Checkpoint current = null;

        for(int i = 0; i < routeList.size(); i++) {
            Checkpoint checkpoint = routeList.get(i);
            if (checkpoint.getPosition() != null) {
                if (order.getCurrentPosition().equals(checkpoint.getPosition())) {
                    current = checkpoint;
                }
            }
            checkpoint.setPosition(i);
            checkpoint.setOrder(order);

            if (checkpoint.getId() != null) {
                checkpointDAO.update(checkpoint);
            } else {
                checkpointDAO.add(checkpoint);
            }
        }

        if (current != null) {
            order.setCurrentPosition(current.getPosition());
        }
    }

    @Override
    @Transactional
    public boolean add(Order order, Cargo cargo) {
        List<Checkpoint> checkpoints = new ArrayList<>();
        checkpoints.add(createCheckpoint(cargo, CheckpointType.LOAD));
        checkpoints.add(createCheckpoint(cargo, CheckpointType.UNLOAD));
        checkCheckpoints(checkpoints);
        add(checkpoints, order);
        addTruckToOrder(order.getTruck(), order);
        return true;
    }

    @Override
    @Transactional
    public void checkCheckpoints(List<Checkpoint> checkpoints) {
        for (Checkpoint checkpoint: checkpoints) {
            Cargo cargo = cargoDAO.getById(checkpoint.getCargo().getId());
            if(checkpoint.getType() != CheckpointType.LOAD) continue;
            if (cargo.getStatus() == CargoStatus.PROCESSING) continue;
            cargo.setStatus(CargoStatus.PROCESSING);
            cargoDAO.update(cargo);
        }
    }

    @Override
    @Transactional
    public void addTruckToOrder(Truck truck, Order order) {
        if (truck.getLocation().getId().equals(order.getStart().getId())) return;
        List<Location> relocationRoute = new ArrayList<>();
        relocationRoute.add(truck.getLocation());
        relocationRoute.addAll(distanceService.getRoute(truck.getLocation(), order.getStart()));
        List<Checkpoint> checkpoints = getTransitCheckpoints(relocationRoute);

        for (int i = 0; i < checkpoints.size(); i++) {
            Checkpoint checkpoint = checkpoints.get(i);
            checkpoint.setPosition(i - checkpoints.size());
            checkpoint.setOrder(order);
            checkpointDAO.add(checkpoint);
        }
        if (!checkpoints.isEmpty()) {
            order.setCurrentPosition(checkpoints.get(0).getPosition());
            order.setStart(truck.getLocation());
        }
    }

    @Override
    public Checkpoint createCheckpoint(Cargo cargo, CheckpointType type) {
        Checkpoint checkpoint = new Checkpoint();
        checkpoint.setCargo(cargo);
        checkpoint.setType(type);
        if (type == CheckpointType.LOAD) {
            checkpoint.setLocation(cargo.getStart());
        } else if (type == CheckpointType.UNLOAD) {
            checkpoint.setLocation(cargo.getEnd());
        } else return null;

        return checkpoint;
    }

    private List<Checkpoint> getTransitCheckpoints(List<Location> route) {
        List<Checkpoint> checkpoints = new ArrayList<>();

        for(Location location: route) {
            Checkpoint checkpoint = new Checkpoint();
            checkpoint.setLocation(location);
            checkpoint.setType(CheckpointType.TRANSIT);
            checkpoints.add(checkpoint);
        }

        return checkpoints;
    }
}
