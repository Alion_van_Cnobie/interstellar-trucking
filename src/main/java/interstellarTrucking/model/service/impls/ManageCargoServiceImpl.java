package interstellarTrucking.model.service.impls;

import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.CargoStatus;
import interstellarTrucking.model.entities.enums.CheckpointType;
import interstellarTrucking.model.entities.enums.OrderStatus;
import interstellarTrucking.model.service.interfaces.*;
import interstellarTrucking.utils.PrivateNumberGenerator;
import org.joda.time.MutableDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.logging.Logger;

import static interstellarTrucking.model.service.interfaces.Params.AFFORDABLE_DELAY;
import static interstellarTrucking.model.service.interfaces.Params.LOADING_DURATION;
import static interstellarTrucking.model.service.interfaces.Params.MAX_WORKED_OUT;

@Service
public class ManageCargoServiceImpl implements ManageCargoService {

    private final CargoService cargoService;
    private final OrderService orderService;
    private final DistanceService distanceService;
    private final CheckpointService checkpointService;
    private final TruckService truckService;
    private final PilotService pilotService;
    private final TruckBoardNotifier truckBoardNotifier;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    public ManageCargoServiceImpl(CargoService cargoService, OrderService orderService, DistanceService distanceService,
                                  CheckpointService checkpointService, TruckService truckService,
                                  PilotService pilotService, TruckBoardNotifier truckBoardNotifier) {
        this.cargoService = cargoService;
        this.orderService = orderService;
        this.distanceService = distanceService;
        this.checkpointService = checkpointService;
        this.truckService = truckService;
        this.pilotService = pilotService;
        this.truckBoardNotifier = truckBoardNotifier;
    }

    @Override
    public void cacheLocations() {
        distanceService.cacheHyperMap();
        distanceService.calculateHyperMap();
        distanceService.writeHyperMap();
    }

    @Override
    @Scheduled(fixedDelay = 20000)
    public void processCargoes() {
        orderService.deleteCreated();
        List<Cargo> cargoes = cargoService.getAllWithStatus(CargoStatus.READY);
        List<Order> orders = orderService.getAllWithStatus(OrderStatus.PROCESSING);

        while (!cargoes.isEmpty()) {
            Cargo maxDistanceCargo = getMaxDistanceCargo(cargoes);
            processCargo(maxDistanceCargo, orders);
            cargoes.remove(maxDistanceCargo);
        }
    }

    private void processCargo(Cargo cargo, List<Order> orders) {
        Order orderTemplate = orderService.getByNumber(createBestOrderTemplate(cargo));
        List<Checkpoint> merged = null;

        for (Order order: orders) {
            List<Checkpoint> mergeTried = checkToInject(cargo, order, orderTemplate);
            if (mergeTried != null) {
                merged = mergeTried;
                orderTemplate = order;
            }
        }

        if (orderTemplate == null) return;
        sendCargo(merged, orderTemplate, cargo);
    }

    private void sendCargo(List<Checkpoint> merged, Order orderTemplate, Cargo cargo) {
        if (merged == null) {
            sendCargo(orderTemplate, cargo);
            return;
        }
        checkpointService.deleteTransitCheckpoints(orderTemplate);
        orderService.update(merged, orderTemplate);
        truckBoardNotifier.sendNotice("Something new appeared!");
    }

    private void sendCargo(Order orderTemplate, Cargo cargo) {
        if (orderTemplate == null) return;
        List<Pilot> pilots = getAppropriatePilots(cargo, orderTemplate.getTruck());
        if (pilots.size() < orderTemplate.getTruck().getShiftSize()) return;
        Set<Pilot> crew = new HashSet<>();
        int crewSize = 0;
        for (Pilot pilot: pilots) {
            crew.add(pilot);
            crewSize++;
            if (crewSize == orderTemplate.getTruck().getShiftSize()) break;
        }
        orderTemplate.setPilots(crew);
        orderService.add(orderTemplate, cargo);
        truckBoardNotifier.sendNotice("Something new appeared!");
    }

    private List<Checkpoint> checkToInject(Cargo cargo, Order order, Order orderTemplate) {
        if (order.getTruck().getCapacity() < cargo.getWeight()) return null;
        List<Checkpoint> checkpoints = checkpointService.getAllByOrder(order);
        List<Checkpoint> unvisited = getAllUnvisitedAndNotTransit(order, checkpoints);

        Checkpoint load = checkpointService.createCheckpoint(cargo, CheckpointType.LOAD);
        Checkpoint unload = checkpointService.createCheckpoint(cargo, CheckpointType.UNLOAD);
        load.setOrder(order);
        unload.setOrder(order);

        Integer delta = injectCheckpoint(load, unvisited);
        delta += injectCheckpoint(unload, unvisited);
        if (!checkInjection(unvisited)) return null;
        Integer restTime = order.getEstimatedTime() - getEstimatedTime(order, checkpoints) + delta;

        for(Pilot pilot: order.getPilots()) {
            if (!checkPilot(pilot, restTime, order.getTruck())) return null;
        }

        Integer waitTime = calculateWaitTime(unvisited, checkpoints, order);
        if (orderTemplate != null && orderTemplate.getStatus() == OrderStatus.CREATED) waitTime -= AFFORDABLE_DELAY;
        if (orderTemplate != null && delta + waitTime > orderTemplate.getEstimatedTime()) return null;
        List<Checkpoint> merged = merge(unvisited, checkpoints);
        Integer maxLoad = getMaxLoad(merged);
        if (maxLoad > order.getTruck().getCapacity()) return null;
        order.setMaxLoad(maxLoad);
        order.setEstimatedTime(order.getEstimatedTime() + delta);
        return merged;
    }

    private List<Checkpoint> merge(List<Checkpoint> unvisited, List<Checkpoint> checkpoints) {
        List<Checkpoint> merged = new ArrayList<>();

        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getType() == CheckpointType.TRANSIT) continue;
            merged.add(checkpoint);
            if (unvisited.indexOf(checkpoint) >= 0) {
                for (int i = 1; i < unvisited.size(); i++) {
                    merged.add(unvisited.get(i));
                }
                break;
            }
        }

        return merged;
    }

    private boolean checkInjection(List<Checkpoint> unvisited) {
        for (Checkpoint checkpoint: unvisited) {
            if (checkpoint.getId() == null && checkpoint.getType() == CheckpointType.UNLOAD) return false;
            if (checkpoint.getId() == null && checkpoint.getType() == CheckpointType.LOAD) return true;
        }
        return false;
    }

    private Integer calculateWaitTime(List<Checkpoint> unvisited, List<Checkpoint> checkpoints, Order order) {
        Checkpoint preInjected = null;
        Checkpoint injected = null;
        Integer waitTime = 0;
        for (Checkpoint unvisitedCheckpoint: unvisited) {
            if (unvisitedCheckpoint.getId() == null) {
                injected = unvisitedCheckpoint;
                break;
            }
            preInjected = unvisitedCheckpoint;
        }

        if (preInjected == null || injected == null) throw new IllegalArgumentException();
        Checkpoint prev = null;

        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getPosition() < order.getCurrentPosition()) continue;
            if (preInjected.getId().equals(checkpoint.getId())) {
                waitTime += LOADING_DURATION;
                waitTime += distanceService.getDistance(preInjected.getLocation(), injected.getLocation());
                break;
            }
            if (prev == null) {
                if (checkpoint.getType() != CheckpointType.TRANSIT && !isAllCargoesLoaded(checkpoint)) {
                    waitTime += LOADING_DURATION;
                }
                prev = checkpoint;
                continue;
            }

            waitTime += LOADING_DURATION;
            waitTime += distanceService.getDistance(prev.getLocation(), checkpoint.getLocation());
            prev = checkpoint;
        }
        return waitTime;
    }

    private Integer injectCheckpoint(Checkpoint inj, List<Checkpoint> checkpoints) {
        if (checkpoints.isEmpty()) return null;

        Checkpoint nearestToNext = null;
        Checkpoint next = null;
        Integer nearestToNextDist = null;
        Checkpoint prevToNearest = null;
        Checkpoint prev = null;
        Integer prevToNearestDist = null;
        Integer delta;

        for (int i = 0; i < checkpoints.size(); i++) {
            Checkpoint checkpoint = checkpoints.get(i);
            if (nearestToNext == null || nearestToNextDist
                    > distanceService.getDistance(checkpoint.getLocation(), inj.getLocation())
                    || distanceService.getDistance(checkpoint.getLocation(), inj.getLocation()).equals(nearestToNextDist)
                    && inj.getType()== CheckpointType.LOAD) {
                nearestToNext = checkpoint;
                next = null;
                nearestToNextDist = distanceService.getDistance(checkpoint.getLocation(), inj.getLocation());

                for (int j = i + 1; j < checkpoints.size(); j++) {
                    Checkpoint nextCheckpoint = checkpoints.get(j);
                    if (nextCheckpoint.getLocation().getId().equals(checkpoint.getLocation().getId())) continue;
                    next = nextCheckpoint;
                    break;
                }
            }

            if (prevToNearest == null || prevToNearestDist
                    >= distanceService.getDistance(inj.getLocation(), checkpoint.getLocation())
                    || distanceService.getDistance(inj.getLocation(), checkpoint.getLocation()).equals(prevToNearestDist)
                    && inj.getType()== CheckpointType.LOAD) {
                prevToNearest = checkpoint;
                prev = null;
                prevToNearestDist = distanceService.getDistance(inj.getLocation(), checkpoint.getLocation());

                for (int j = i -1; j >= 0; j--) {
                    Checkpoint prevCheckpoint = checkpoints.get(j);
                    if (prevCheckpoint.getLocation().getId().equals(checkpoint.getLocation().getId())) continue;
                    prev = prevCheckpoint;
                    break;
                }
            }
        }

        if (nearestToNextDist.equals(0) && inj.getType()== CheckpointType.LOAD) {
            int index = checkpoints.indexOf(nearestToNext);
            if (index < checkpoints.size()) {
                checkpoints.add(index+1, inj);
            } else checkpoints.add(inj);
            return LOADING_DURATION;
        }

        if (nearestToNextDist.equals(0) && inj.getType()== CheckpointType.UNLOAD) {
            int index = checkpoints.indexOf(nearestToNext);
            checkpoints.add(index, inj);
            return LOADING_DURATION;
        }

        if (prev != null) {
            Integer prevToNearestDelta = calculateDelta(prev.getLocation(), inj.getLocation(), prevToNearest.getLocation());
            Integer nearestToNextDelta;
            if (next != null) {
                nearestToNextDelta = calculateDelta(nearestToNext.getLocation(), inj.getLocation(), next.getLocation());
            } else {
                nearestToNextDelta = calculateDelta(null, inj.getLocation(), nearestToNext.getLocation());
            }
            if (prevToNearestDelta < nearestToNextDelta) {
                checkpoints.add(checkpoints.indexOf(prevToNearest), inj);
                delta = prevToNearestDelta + LOADING_DURATION;
            } else {
                if (next != null) {
                    checkpoints.add(checkpoints.indexOf(next), inj);
                } else {
                    checkpoints.add(inj);
                }
                delta = nearestToNextDelta + LOADING_DURATION;
            }

        } else if (next != null) {
            Integer nearestToNextDelta = calculateDelta(nearestToNext.getLocation(), inj.getLocation(), next.getLocation());
            checkpoints.add(checkpoints.indexOf(next), inj);
            delta = nearestToNextDelta + LOADING_DURATION;
        } else {
            Integer nearestToNextDelta = calculateDelta(null, inj.getLocation(), nearestToNext.getLocation());
            checkpoints.add(inj);
            delta = nearestToNextDelta + LOADING_DURATION;
        }
        return delta;
    }

    private Integer calculateDelta(Location prev, Location point, Location next) {
        if (prev == null) {
            return distanceService.getDistance(point, next);
        }
        return distanceService.getDistance(prev, point)
                + distanceService.getDistance(point, next)
                - distanceService.getDistance(prev, next);
    }

    private List<Checkpoint> getAllUnvisitedAndNotTransit(Order order, List<Checkpoint> checkpoints) {
        List<Checkpoint> unvisited = new ArrayList<>();

        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getType() == CheckpointType.TRANSIT) continue;
            if (checkpoint.getPosition() < order.getCurrentPosition()) continue;
            if (checkpoint.getPosition().equals(order.getCurrentPosition())
                    && isAllCargoesLoaded(checkpoint)) continue;
            unvisited.add(checkpoint);
        }

        return unvisited;
    }

    private boolean isAllCargoesLoaded(Checkpoint checkpoint) {
        if (checkpoint.getType() == CheckpointType.LOAD
                && checkpoint.getCargo().getStatus() == CargoStatus.LOADED) return true;
        if (checkpoint.getType() == CheckpointType.UNLOAD
                && checkpoint.getCargo().getStatus() == CargoStatus.DELIVERED) return true;
        return false;
    }

    private String createBestOrderTemplate(Cargo cargo) {
        List<Truck> trucks = truckService.getAllReadyWithCapacity(cargo.getWeight());
        while (!trucks.isEmpty()) {
            Truck truck = getTruckMinTime(trucks, cargo);
            List<Pilot> pilots = getAppropriatePilots(cargo, truck);
            if (pilots.size() < truck.getShiftSize() || cargo.getWeight()> truck.getCapacity()) {
                trucks.remove(truck);
                continue;
            }

            Order order = new Order();
            order.setTruck(truck);
            order.setPilots(new HashSet<>(pilots));
            order.setEstimatedTime(getEstimatedTime(cargo, truck));
            order.setMaxLoad(cargo.getWeight());
            order.setStart(cargo.getStart());
            order.setFinish(cargo.getEnd());
            String number;
            do {
                number = PrivateNumberGenerator.generatePrivateNumber();
            } while (orderService.getByNumber(number) != null );
            order.setUniqueNumber(number);
            order.setCurrentPosition(0);
            orderService.create(order);
            return number;
        }
        return null;
    }

    private List<Pilot> getAppropriatePilots(Cargo cargo, Truck truck) {
        List<Pilot> pilots = pilotService.getAllFreeByLocation(truck.getLocation());
        List<Pilot> appropriatePilots = new ArrayList<>();

        for (Pilot pilot: pilots) {
            if (checkPilot(pilot, cargo, truck)) appropriatePilots.add(pilot);
        }

        return appropriatePilots;
    }

    private boolean checkPilot(Pilot pilot, Cargo cargo, Truck truck) {
        Integer estimatedTime = getEstimatedTime(cargo, truck);
        return checkPilot(pilot, estimatedTime, truck);
    }

    private boolean checkPilot(Pilot pilot, Integer estimatedTime, Truck truck) {
        int hours = getHoursToNextMonth();
        int hoursOfNextMonth = getHoursOfNextMonth();
        int workHoursInNextMonth = (estimatedTime - hours > hoursOfNextMonth) ? hoursOfNextMonth : estimatedTime - hours;
        hours = (hours > estimatedTime) ? estimatedTime : hours;
        return hours/truck.getShiftSize() < MAX_WORKED_OUT - pilot.getWorkedOut()/60
                && workHoursInNextMonth/truck.getShiftSize() < MAX_WORKED_OUT;
    }

    private Integer getEstimatedTime(Cargo cargo, Truck truck) {
        return distanceService.getDistance(truck.getLocation(), cargo.getStart())
                + distanceService.getDistance(cargo.getStart(), cargo.getEnd())
                + LOADING_DURATION*2;
    }

    private Integer getEstimatedTime(Order order, List<Checkpoint> checkpoints) {
        Checkpoint prev = null;
        Integer time = 0;
        for (Checkpoint checkpoint: checkpoints) {
            if (order != null && order.getCurrentPosition().equals(checkpoint.getPosition())) break;
            if (checkpoint.getType() != CheckpointType.TRANSIT) {
                time += LOADING_DURATION;
            }

            if (prev == null) {
                prev = checkpoint;
                continue;
            }

            time += distanceService.getDistance(prev.getLocation(), checkpoint.getLocation());
            prev = checkpoint;
        }

        return time;
    }

    private int getHoursToNextMonth() {
        MutableDateTime mdt = new MutableDateTime();
        mdt.addMonths(1);
        mdt.setDayOfMonth(1);
        mdt.setMillisOfDay(0);
        return (int)(mdt.getMillis() - new MutableDateTime().getMillis())/3600000;
    }

    private int getHoursOfNextMonth() {
        MutableDateTime mdt = new MutableDateTime();
        mdt.addMonths(1);
        mdt.setDayOfMonth(1);
        mdt.setMillisOfDay(0);
        return mdt.dayOfMonth().getMaximumValue()*24;
    }

    private Integer getMaxLoad(List<Checkpoint> checkpoints) {
        Integer maxLoad = 0;
        Integer currentLoad = 0;
        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getType() == CheckpointType.LOAD) currentLoad += checkpoint.getCargo().getWeight();
            if (checkpoint.getType() == CheckpointType.UNLOAD) currentLoad -= checkpoint.getCargo().getWeight();
            if (maxLoad < currentLoad) maxLoad = currentLoad;
        }
        return maxLoad;
    }

    private Truck getTruckMinTime(List<Truck> trucks, Cargo cargo) {
        Truck truckMinTime = null;

        for(Truck truck : trucks) {
            if (truckMinTime == null || distanceService.getDistance(truckMinTime.getLocation(), cargo.getStart())
                    > distanceService.getDistance(truck.getLocation(), cargo.getStart())) {
                truckMinTime = truck;
            }
        }

        return truckMinTime;
    }

    private Cargo getMaxDistanceCargo(List<Cargo> cargoes) {
        Cargo maxDistanceCargo = null;

        for (Cargo cargo : cargoes) {
            if (maxDistanceCargo == null
                    || distanceService.getDistance(maxDistanceCargo.getStart(), maxDistanceCargo.getEnd())
                    < distanceService.getDistance(cargo.getStart(), cargo.getEnd())) {
                maxDistanceCargo = cargo;
            }
        }

        return maxDistanceCargo;
    }
}
