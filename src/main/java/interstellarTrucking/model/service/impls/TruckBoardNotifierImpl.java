package interstellarTrucking.model.service.impls;

import interstellarTrucking.model.service.interfaces.TruckBoardNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;


@Service
public class TruckBoardNotifierImpl implements TruckBoardNotifier {

    private final JmsTemplate jmsTemplate;

    @Autowired
    public TruckBoardNotifierImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendNotice(String notice) {
        jmsTemplate.send(session -> session.createObjectMessage(notice));
    }
}
