package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.DistanceDAO;
import interstellarTrucking.DAO.interfaces.HyperwayDAO;
import interstellarTrucking.model.DTO.DistanceCache;
import interstellarTrucking.model.DTO.LocationCache;
import interstellarTrucking.model.entities.Distance;
import interstellarTrucking.model.entities.Hyperway;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.service.interfaces.DistanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class DistanceServiceImpl implements DistanceService {

    private final DistanceDAO distanceDAO;
    private final HyperwayDAO hyperwayDAO;
    private final Map<Location, LocationCache> hyperMap = new HashMap<>();
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @Autowired
    public DistanceServiceImpl(DistanceDAO distanceDAO, HyperwayDAO hyperwayDAO) {
        this.distanceDAO = distanceDAO;
        this.hyperwayDAO = hyperwayDAO;
    }

    @Override
    public List<Location> getRoute(Location start, Location end) {
        List<Location> route = new ArrayList<>();

        while (true) {
            DistanceCache distanceCache = hyperMap.get(start).getDistances().get(end);
            end = distanceCache.getPrev();
            if (start.equals(end)) break;
            route.add(end);
        }

        Collections.reverse(route);
        return route;
    }

    @Override
    public Integer getDistance(Location start, Location end) {
        if (start.getId().equals(end.getId())) return 0;
        return hyperMap.get(start).getDistance(end);
    }

    @Override
    @Transactional(readOnly = true)
    public void cacheHyperMap() {
        cacheHyperways();
        cacheDistances();
    }

    @Override
    public void calculateHyperMap() {
        for (Location location: hyperMap.keySet()) {
            calculateLocation(location);
        }
    }

    @Override
    @Transactional
    public void writeHyperMap() {
        for (Map.Entry<Location, LocationCache> entry : hyperMap.entrySet()) {
            Location start = entry.getKey();
            for (Map.Entry<Location, DistanceCache> locationEntry : entry.getValue().getDistances().entrySet()) {
                Location finish = locationEntry.getKey();
                DistanceCache distanceCache = locationEntry.getValue();
                Distance distance = distanceDAO.getById(start, finish);
                if (distance == null) {
                    distance = new Distance();
                    distance.setStart(start);
                    distance.setFinish(finish);
                    distance.setDistance(distanceCache.getDistance());
                    distance.setBypassNumber(distanceCache.getNumber());
                    distance.setPrev(distanceCache.getPrev());
                    distanceDAO.add(distance);
                }
                if (distance.getDistance().equals(distanceCache.getDistance())
                        && distance.getBypassNumber().equals(distanceCache.getNumber())) continue;
                distance.setDistance(distanceCache.getDistance());
                distance.setBypassNumber(distanceCache.getNumber());
                distanceDAO.update(distance);
            }
        }
    }

    private void cacheHyperways() {
        List<Hyperway> hyperways = hyperwayDAO.getAll();

        for (Hyperway hyperway : hyperways) {
            LocationCache locationCache = hyperMap.get(hyperway.getStart());
            if (locationCache == null) {
                locationCache = new LocationCache();
                hyperMap.put(hyperway.getStart(), locationCache);
            }

            locationCache.addHyperway(hyperway);
        }
    }

    private void cacheDistances() {
        List<Distance> distances = distanceDAO.getAll();

        for (Distance distance : distances) {
            LocationCache locationCache = hyperMap.get(distance.getStart());
            if (locationCache == null) throw new IllegalArgumentException("No cache for location: " + distance.getStart().getName());

            locationCache.addDistance(distance);
        }
    }

    private void calculateLocation(Location location) {
        logger.log(Level.INFO, "Start calculate: " + location.getName());
        Map<Location, DistanceCache> distances = hyperMap.get(location).getDistances();
        Map<Location, Integer> hyperways = hyperMap.get(location).getHyperways();

        for (Location finish : hyperways.keySet()) {
            if (distances.get(finish) != null) continue;
            distances.put(finish, new DistanceCache(hyperways.get(finish), null, location));
        }

        Location shortest = findShortestUncalculated(distances);
        while (shortest != null) {
            calculateNearest(shortest, location, distances);
            shortest = findShortestUncalculated(distances);
        }
        logger.log(Level.INFO, "End calculate: " + location.getName());
    }

    private void calculateNearest(Location shortest, Location location, Map<Location, DistanceCache> distances) {
        Map<Location, Integer> hyperways = hyperMap.get(shortest).getHyperways();
        Integer shortestDistance = distances.get(shortest).getDistance();

        for (Map.Entry<Location, Integer> entry : hyperways.entrySet()) {
            if (location.equals(entry.getKey())) continue;
            DistanceCache distanceCache = distances.get(entry.getKey());
            Integer distance = shortestDistance + entry.getValue();
            if (distanceCache == null) {
                distanceCache = new DistanceCache(distance, null, shortest);
                distances.put(entry.getKey(), distanceCache);
            } else {
                if (distanceCache.getNumber() != null) continue;
                if (distance < distanceCache.getDistance()) {
                    distanceCache.setDistance(distance);
                    distanceCache.setPrev(shortest);
                }
            }
        }
    }

    private Location findShortestUncalculated(Map<Location, DistanceCache> distances) {
        Location shortest = null;
        Integer distance = null;
        Integer maxNumber = 0;
        for (Map.Entry<Location, DistanceCache> entry : distances.entrySet()) {
            if (entry.getValue().getNumber() != null) {
                if (maxNumber < entry.getValue().getNumber()) {
                    maxNumber = entry.getValue().getNumber();
                }
                continue;
            }

            if (shortest == null) {
                shortest = entry.getKey();
                distance = entry.getValue().getDistance();
                continue;
            }

            if (distance > entry.getValue().getDistance()) {
                shortest = entry.getKey();
                distance = entry.getValue().getDistance();
            }
        }

        if (shortest == null) return null;
        distances.get(shortest).setNumber(maxNumber+1);
        return shortest;
    }
}
