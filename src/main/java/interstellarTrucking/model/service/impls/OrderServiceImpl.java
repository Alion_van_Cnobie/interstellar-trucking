package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.OrderDAO;
import interstellarTrucking.model.DTO.OrderDTO;
import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.*;
import interstellarTrucking.model.service.interfaces.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class OrderServiceImpl implements OrderService {
    private final OrderDAO<Order> orderDAO;
    private final CargoService cargoService;
    private final CheckpointService checkpointService;
    private final TruckService truckService;
    private final PilotService pilotService;

    @Autowired
    public OrderServiceImpl(OrderDAO<Order> orderDAO, CargoService cargoService, CheckpointService checkpointService,
                            TruckService truckService, PilotService pilotService) {
        this.orderDAO = orderDAO;
        this.cargoService = cargoService;
        this.checkpointService = checkpointService;
        this.truckService = truckService;
        this.pilotService = pilotService;
    }

    @Override
    @Transactional
    public void add(Order order, Cargo cargo) {
        checkpointService.add(order, cargo);
        truckService.setStatus(order.getTruck(), TruckStatus.BUSY);
        for (Pilot pilot: order.getPilots()) {
            pilotService.setStatus(pilot, PilotStatus.ASSIGNED);
            pilotService.setTruck(pilot, order.getTruck());

        }
        order.setStatus(OrderStatus.PROCESSING);
        order.setStart(order.getTruck().getLocation());
        orderDAO.update(order);
    }

    @Override
    @Transactional
    public void create(Order order) {
        order.setStatus(OrderStatus.CREATED);
        orderDAO.add(order);
    }

    @Override
    @Transactional(readOnly = true)
    public Order getByPilot(Pilot pilot) {
        Truck truck = pilot.getTruck();
        if (truck == null) return null;
        Order order = getByTruck(truck);
        if (order != null) return order;
        return getLastByTruck(truck);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllWithoutStatus(OrderStatus status) {
        return orderDAO.getAllWithoutStatus(status);
    }

    @Override
    @Transactional
    public void delete(Order order) {
        orderDAO.delete(order);
    }

    @Override
    @Transactional
    public void deleteCreated() {
        List<Order> orders = orderDAO.getAllWithStatus(OrderStatus.CREATED);
        for (Order order: orders) {
            orderDAO.delete(order);
        }
    }

    @Override
    @Transactional
    public void update(Order order) {
        orderDAO.update(order);
    }

    @Override
    @Transactional
    public void update(List<Checkpoint> merged, Order orderTemplate) {
        checkpointService.checkCheckpoints(merged);
        checkpointService.add(merged, orderTemplate);
        orderTemplate.setFinish(merged.get(merged.size()-1).getLocation());
        orderDAO.update(orderTemplate);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAll() {
        return orderDAO.getAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Order getById(Integer id) {
        return orderDAO.getById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Order getByNumber(String number) {
        return orderDAO.getByNumber(number);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllWithStatus(OrderStatus status) {
        return orderDAO.getAllWithStatus(status);
    }

    @Override
    @Transactional(readOnly = true)
    public Order getByTruck(Truck truck) {
        List<Order> orders = orderDAO.getByTruck(truck);

        for (Order order: orders) {
            if (order.getStatus() == OrderStatus.READY || order.getStatus() == OrderStatus.TRUCK_ASSIGNED
                    || order.getStatus() == OrderStatus.PROCESSING) {
                return order;
            }
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public Order getLastByTruck(Truck truck) {
        List<Order> orders = orderDAO.getByTruck(truck);
        return (orders.isEmpty()) ? null : orders.get(0);
    }

    @Override
    @Transactional
    public void complete(Order order) {
        order.setStatus(OrderStatus.DONE);
        orderDAO.update(order);
    }

    @Override
    @Transactional
    public void setPosition(Order order, Integer position) {
        order.setCurrentPosition(position);
        orderDAO.update(order);
    }

    @Override
    @Transactional
    public void interrupt(Order order) {
        order.setStatus(OrderStatus.INTERRUPTED);
        orderDAO.update(order);
        List<Checkpoint> checkpoints = checkpointService.getAllByOrder(order);
        Checkpoint current = checkpointService.getByOrderAndPosition(order, order.getCurrentPosition());

        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getType() != CheckpointType.LOAD) continue;
            Cargo cargo = checkpoint.getCargo();
            if (cargo.getStatus() == CargoStatus.LOADED) {
                if (cargo.getEnd().getId().equals(current.getLocation().getId())) {
                    cargoService.setStatus(cargo, CargoStatus.DELIVERED);
                } else {
                    cargoService.emergencyUnload(cargo, current.getLocation());
                }
            }

            if (cargo.getStatus() == CargoStatus.PROCESSING) {
                cargoService.setStatus(cargo, CargoStatus.READY);
            }
        }

        truckService.setStatus(order.getTruck(), TruckStatus.BROKEN);
    }

    @Override
    public List<OrderDTO> convertEntityListToDTO(List<Order> orders) {
        List<OrderDTO> orderDTOList = new ArrayList<>();

        for (Order order: orders) {
            orderDTOList.add(convertEntityToDTO(order));
        }

        return orderDTOList;
    }

    @Override
    public OrderDTO convertEntityToDTO(Order order) {
        OrderDTO orderDTO = new OrderDTO(order);
        orderDTO.setCurrent(checkpointService.getByOrderAndPosition(order, order.getCurrentPosition()).getLocation().getName());
        calculateCargoesForDTO(orderDTO, order);
        return orderDTO;
    }

    private void calculateCargoesForDTO(OrderDTO orderDTO, Order order) {
        Integer notLoaded = 0;
        Integer loaded = 0;
        Integer delivered = 0;

        List<Checkpoint> checkpoints = checkpointService.getAllByOrder(order);

        for (Checkpoint checkpoint: checkpoints) {
            if (checkpoint.getType() == CheckpointType.TRANSIT) continue;
            if (checkpoint.getCargo().getStatus() == CargoStatus.PROCESSING) notLoaded++;
            if (checkpoint.getCargo().getStatus() == CargoStatus.LOADED) loaded++;
            if (checkpoint.getCargo().getStatus() == CargoStatus.DELIVERED) delivered++;
        }

        orderDTO.setNotLoaded(notLoaded);
        orderDTO.setLoaded(loaded);
        orderDTO.setDelivered(delivered);
    }
}
