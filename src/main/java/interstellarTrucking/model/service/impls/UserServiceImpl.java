package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.UserDAO;
import interstellarTrucking.model.entities.User;
import interstellarTrucking.model.entities.enums.UserRole;
import interstellarTrucking.model.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserDAO<User> userDAO;

    @Autowired
    public UserServiceImpl(UserDAO<User> userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public User getByUsername(String username) {
        return userDAO.getByUsername(username);
    }

    @Override
    public User getCurrentUser() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return getByUsername(username);
    }

    @Override
    public User updateCurrentUser(String username, String password) {
        String currentUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        if (!currentUsername.equals(username) && getByUsername(username) != null) return null;
        User user = getCurrentUser();
        user.setLogin(username);
        user.setPassword(passwordEncoder().encode(password));
        update(user);
        return user;
    }

    @Override
    public void update(User user) {
       userDAO.update(user);
    }

    @Override
    public void delete(User user, boolean full) {
        if (full) userDAO.delete(user);
        else {
            user.setEnabled(false);
            update(user);
        }
    }

    @Override
    public User createUser(String username, String password, UserRole role) {
        if (getByUsername(username) != null) return null;
        User user = new User();
        user.setLogin(username);
        user.setPassword(passwordEncoder().encode(password));
        user.setRole(role);
        user.setEnabled(true);
        userDAO.add(user);
        return user;
    }

    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

}
