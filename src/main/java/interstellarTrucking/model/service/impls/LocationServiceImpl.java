package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.service.interfaces.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class LocationServiceImpl implements LocationService {

    private final LocationDAO<Location> locationDAO;

    @Autowired
    public LocationServiceImpl(LocationDAO<Location> locationDAO) {
        this.locationDAO = locationDAO;
    }

    @Override
    public List<Location> getAll() {
        return locationDAO.getAll();
    }

    @Override
    public Location getById(Integer id) {
         return locationDAO.getById(id);
    }

}
