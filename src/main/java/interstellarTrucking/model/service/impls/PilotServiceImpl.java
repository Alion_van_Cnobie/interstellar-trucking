package interstellarTrucking.model.service.impls;

import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.DAO.interfaces.PilotDAO;
import interstellarTrucking.model.DTO.PilotDTO;
import interstellarTrucking.model.entities.*;
import interstellarTrucking.model.entities.enums.PilotStatus;
import interstellarTrucking.model.entities.enums.UserRole;
import interstellarTrucking.model.service.interfaces.PilotService;
import interstellarTrucking.model.service.interfaces.UserService;
import interstellarTrucking.utils.PrivateNumberGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class PilotServiceImpl implements PilotService {

    private final PilotDAO<Pilot> pilotDAO;
    private final UserService userService;
    private final LocationDAO<Location> locationDAO;

    @Autowired
    public PilotServiceImpl(PilotDAO<Pilot> pilotDAO, UserService userService, LocationDAO<Location> locationDAO) {
        this.pilotDAO = pilotDAO;
        this.userService = userService;
        this.locationDAO = locationDAO;
    }

    @Override
    public String add(PilotDTO pilotDTO) {
        Pilot pilot = new Pilot();
        pilot.setFirstName(pilotDTO.getFirstName());
        pilot.setLastName(pilotDTO.getLastName());
        Location location = locationDAO.getByName(pilotDTO.getLocation());
        if (location == null ) throw new IllegalArgumentException("No such location: " + pilotDTO.getLocation());
        pilot.setLocation(location);
        pilot.setStatus(PilotStatus.NEW);

        String number;
        User user;

        do {
            number = PrivateNumberGenerator.generatePrivateNumber();
        } while (pilotDAO.getByNumber(number) != null || (user =
                userService.createUser(number, number, UserRole.PILOT)) == null);

        pilot.setPrivateNumber(number);
        pilot.setUser(user);
        pilot.setValid(true);
        pilot.setWorkedOut(0);
        pilot.setChangeStatusTime(new Date());

        pilotDAO.add(pilot);
        return pilot.getPrivateNumber();
    }

    @Override
    public void delete(String number) {
        Pilot pilot = pilotDAO.getByNumber(number);
        if (pilot == null) return;
        if (!pilot.getValid()) return;
        if (pilot.getStatus() == PilotStatus.NEW) {
            userService.delete(pilot.getUser(), true);
            pilotDAO.delete(pilot);
            return;
        }

        if (pilot.getStatus() != PilotStatus.FIRED && pilot.getStatus() != PilotStatus.LOST) return;

        pilot.setValid(false);
        userService.delete(pilot.getUser(), false);
        pilotDAO.update(pilot);
    }

    @Override
    public void update(String number, PilotDTO pilotDTO) {
        Pilot oldPilot = pilotDAO.getByNumber(number);
        if (oldPilot == null) return;
        if (!oldPilot.getValid()) return;

        oldPilot.setFirstName(pilotDTO.getFirstName());
        oldPilot.setLastName(pilotDTO.getLastName());
        if (pilotDTO.getStatus().isEmpty()) throw new IllegalArgumentException("No status");
        if (pilotDTO.getLocation().isEmpty()) throw new IllegalArgumentException("No location");
        if (pilotDTO.getWorkedOut() == null) throw new IllegalArgumentException("No worked out hours");
        Location location = locationDAO.getByName(pilotDTO.getLocation());
        if (location == null ) throw new IllegalArgumentException("No such location: " + pilotDTO.getLocation());
        oldPilot.setStatus(PilotStatus.valueOf(pilotDTO.getStatus()));
        oldPilot.setLocation(location);
        oldPilot.setWorkedOut(pilotDTO.getWorkedOut()*60);
        pilotDAO.update(oldPilot);
    }


    @Override
    public void update(Pilot pilot) {
        pilotDAO.update(pilot);
    }

    @Override
    public List<Pilot> getAll() {
        return pilotDAO.getAll();
    }

    @Override
    public List<Pilot> getAllValid() {
        return pilotDAO.getAllValid();
    }

    @Override
    public Pilot getById(Integer id) {
        return pilotDAO.getById(id);
    }

    @Override
    public PilotDTO getByNumber(String number) {
        Pilot pilot = pilotDAO.getByNumber(number);
        return (pilot == null) ? null : new PilotDTO(pilot);
    }

    @Override
    public List<Pilot> getAllFreeByLocation(Location location) {
        return pilotDAO.getAllFreeByLocation(location);
    }

    @Override
    public Pilot getByUser(User user){
        return pilotDAO.getByUser(user);
    }

    @Override
    public Pilot getCurrentPilot() {
        return getByUser(userService.getCurrentUser());
    }

    @Override
    public List<Pilot> getAllValidWithStatus(PilotStatus status) {
        return pilotDAO.getAllValidWithStatus(status);
    }

    @Override
    public List<Pilot> getAllValidWithoutStatus(PilotStatus status) {
        return pilotDAO.getAllValidWithoutStatus(status);
    }

    @Override
    public void setStatus(Pilot pilot, PilotStatus status) {
        pilot.setStatus(status);
        pilotDAO.update(pilot);
    }

    @Override
    public void setLocation(Pilot pilot, Location location) {
        pilot.setLocation(location);
        pilotDAO.update(pilot);
    }

    @Override
    public void setTruck(Pilot pilot, Truck truck) {
        pilot.setTruck(truck);
        pilotDAO.update(pilot);
    }

    @Override
    public List<PilotDTO> convertEntityListToDTO(List<Pilot> pilots) {
        List<PilotDTO> pilotDTOList = new ArrayList<>();

        for (Pilot pilot : pilots) {
            pilotDTOList.add(new PilotDTO(pilot));
        }

        return pilotDTOList;
    }
}
