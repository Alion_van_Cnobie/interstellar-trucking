package interstellarTrucking.model.service.impls;

import interstellarTrucking.model.DTO.BoardDTO;
import interstellarTrucking.model.entities.Order;
import interstellarTrucking.model.entities.enums.OrderStatus;
import interstellarTrucking.model.entities.enums.PilotStatus;
import interstellarTrucking.model.entities.enums.TruckStatus;
import interstellarTrucking.model.service.interfaces.BoardService;
import interstellarTrucking.model.service.interfaces.OrderService;
import interstellarTrucking.model.service.interfaces.PilotService;
import interstellarTrucking.model.service.interfaces.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class BoardServiceImpl implements BoardService {

    private final OrderService orderService;
    private final PilotService pilotService;
    private final TruckService truckService;
    private static final int ORDER_COUNT = 20;

    @Autowired
    public BoardServiceImpl(OrderService orderService, PilotService pilotService, TruckService truckService) {
        this.orderService = orderService;
        this.pilotService = pilotService;
        this.truckService = truckService;
    }

    @Override
    public BoardDTO getBoardDTO() {
        BoardDTO boardDTO = new BoardDTO();
        boardDTO.setOrders(orderService.convertEntityListToDTO(getOrdersForBoard()));
        boardDTO.setAvailablePilots(pilotService.getAllValidWithStatus(PilotStatus.FREE).size());
        boardDTO.setUnavailablePilots(pilotService.getAllValidWithoutStatus(PilotStatus.FREE).size());
        boardDTO.setFreeTrucks(truckService.getAllValidWithStatus(TruckStatus.READY).size());
        boardDTO.setBusyTrucks(truckService.getAllValidWithStatus(TruckStatus.BUSY).size());
        boardDTO.setBrokenTrucks(truckService.getAllValidWithStatus(TruckStatus.BROKEN).size());
        return boardDTO;
    }

    private List<Order> getOrdersForBoard() {
        List<Order> orders = orderService.getAllWithoutStatus(OrderStatus.CREATED);
        if (orders.size() <= ORDER_COUNT) return orders;
        return orders.subList(0, ORDER_COUNT);
    }
}
