package interstellarTrucking.model.events;

import org.springframework.context.ApplicationEvent;

public class ProcessCargoesEvent extends ApplicationEvent {
    public ProcessCargoesEvent(Object source) {
        super(source);
    }
}
