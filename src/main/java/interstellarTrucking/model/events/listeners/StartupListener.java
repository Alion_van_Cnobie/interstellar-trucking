package interstellarTrucking.model.events.listeners;

import interstellarTrucking.model.service.interfaces.ManageCargoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StartupListener {
    private final ManageCargoService manageCargoService;

    @Autowired
    public StartupListener(ManageCargoService manageCargoService) {

        this.manageCargoService = manageCargoService;
    }

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        manageCargoService.cacheLocations();
    }
}
