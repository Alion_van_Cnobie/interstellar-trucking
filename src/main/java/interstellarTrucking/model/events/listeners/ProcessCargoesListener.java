package interstellarTrucking.model.events.listeners;

import interstellarTrucking.model.events.ProcessCargoesEvent;
import interstellarTrucking.model.service.interfaces.ManageCargoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ProcessCargoesListener implements ApplicationListener<ProcessCargoesEvent> {

    private final ManageCargoService manageCargoService;

    @Autowired
    public ProcessCargoesListener(ManageCargoService manageCargoService) {
        this.manageCargoService = manageCargoService;
    }

    @Override
    public void onApplicationEvent(ProcessCargoesEvent processCargoesEvent) {
        manageCargoService.processCargoes();
    }
}
