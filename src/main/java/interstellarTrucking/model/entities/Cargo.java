package interstellarTrucking.model.entities;

import interstellarTrucking.model.entities.enums.CargoStatus;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cargoes")
public class Cargo {

    private Integer id;
    private String number;
    private String name;
    private Integer weight;
    private Location start;
    private Location end;
    private CargoStatus status;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @Column(name = "number", nullable = false, unique = true, length = 45)
    public String getNumber() {
        return number;
    }

    @Column(name = "name", nullable = false, length = 128)
    public String getName() {
        return name;
    }

    @Column(name = "weight", nullable = false)
    public Integer getWeight() {
        return weight;
    }

    @ManyToOne
    @JoinColumn(name = "start_location_id", nullable = false)
    public Location getStart() {
        return start;
    }

    @ManyToOne
    @JoinColumn(name = "end_location_id", nullable = false)
    public Location getEnd() {
        return end;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status", nullable = false)
    public CargoStatus getStatus() {
        return status;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public void setEnd(Location end) {
        this.end = end;
    }

    public void setStatus(CargoStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cargo)) return false;
        Cargo cargo = (Cargo) o;
        return Objects.equals(id, cargo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
