package interstellarTrucking.model.entities;


import interstellarTrucking.model.entities.enums.OrderStatus;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
public class Order {

    private Integer id;
    private String uniqueNumber;
    private OrderStatus status;
    private Truck truck;
    private Integer estimatedTime;
    private Integer maxLoad;
    private Set<Pilot> pilots = new HashSet<>();
    private Location start;
    private Location finish;
    private Integer currentPosition;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @Column(name = "number", nullable = false, unique = true)
    public String getUniqueNumber() {
        return uniqueNumber;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status", nullable = false)
    public OrderStatus getStatus() {
        return status;
    }

    @ManyToOne
    @JoinColumn(name = "truck_id")
    public Truck getTruck() {
        return truck;
    }

    @Column(name = "estimated_time", nullable = false)
    public Integer getEstimatedTime() {
        return estimatedTime;
    }

    @Column(name = "max_load", nullable = false)
    public Integer getMaxLoad() {
        return maxLoad;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "orders_log",
            joinColumns = { @JoinColumn(name = "order_id") },
            inverseJoinColumns = { @JoinColumn(name = "pilot_id") }
    )
    public Set<Pilot> getPilots() {
        return pilots;
    }

    @ManyToOne
    @JoinColumn(name = "start_location_id")
    public Location getStart() {
        return start;
    }

    @ManyToOne
    @JoinColumn(name = "end_location_id")
    public Location getFinish() {
        return finish;
    }

    @Column(name = "current_position")
    public Integer getCurrentPosition() {
        return currentPosition;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUniqueNumber(String number) {
        this.uniqueNumber = number;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public void setEstimatedTime(Integer estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public void setPilots(Set<Pilot> pilots) {
        this.pilots = pilots;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public void setFinish(Location finish) {
        this.finish = finish;
    }

    public void setMaxLoad(Integer maxLoad) {
        this.maxLoad = maxLoad;
    }

    public void setCurrentPosition(Integer currentPosition) {
        this.currentPosition = currentPosition;
    }
}
