package interstellarTrucking.model.entities;


import interstellarTrucking.model.entities.enums.PilotStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "pilots")
public class Pilot {

    private Integer id;
    private String firstName;
    private String lastName;
    private String privateNumber;
    private Truck truck;
    private PilotStatus status;
    private Integer workedOut;
    private Location location;
    private User user;
    private Boolean valid;
    private Date changeStatusTime;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    @Column(name = "number", nullable = false, unique = true)
    public String getPrivateNumber() {
        return privateNumber;
    }

    @ManyToOne
    @JoinColumn(name = "truck_id")
    public Truck getTruck() {
        return truck;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status", nullable = false)
    public PilotStatus getStatus() {
        return status;
    }

    @Column(name = "worked_out", nullable = false)
    public Integer getWorkedOut() {
        return workedOut;
    }

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    public Location getLocation() {
        return location;
    }

    @OneToOne
    @JoinColumn(name = "user_id", unique = true)
    public User getUser() {
        return user;
    }

    @Column(name = "valid")
    public Boolean getValid() {
        return valid;
    }

    @Column(name = "status_change_time", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getChangeStatusTime() {
        return changeStatusTime;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }

    public void setLastName(String surname) {
        this.lastName = surname;
    }

    public void setPrivateNumber(String number) {
        this.privateNumber = number;
    }

    public void setStatus(PilotStatus status) {
        this.status = status;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public void setWorkedOut(Integer workedOut) {
        this.workedOut = workedOut;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public void setChangeStatusTime(Date changeStatusTime) {
        this.changeStatusTime = changeStatusTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pilot)) return false;
        Pilot pilot = (Pilot) o;
        return Objects.equals(id, pilot.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
