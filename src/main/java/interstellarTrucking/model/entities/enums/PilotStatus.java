package interstellarTrucking.model.entities.enums;

public enum PilotStatus {
    FREE, PILOT, REST, LOADING, DAY_OFF, NEW, ASSIGNED, FIRED, LOST
}
