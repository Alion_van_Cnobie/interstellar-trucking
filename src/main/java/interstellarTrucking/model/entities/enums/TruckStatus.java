package interstellarTrucking.model.entities.enums;

public enum TruckStatus {
    READY, BUSY, BROKEN, LOST, REMOVED
}
