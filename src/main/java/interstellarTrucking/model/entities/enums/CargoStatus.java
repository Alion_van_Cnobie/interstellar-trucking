package interstellarTrucking.model.entities.enums;

public enum CargoStatus {
    READY, LOADED, DELIVERED, LOST, PROCESSING, NEW
}
