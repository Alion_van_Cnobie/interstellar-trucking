package interstellarTrucking.model.entities.enums;

public enum UserRole {
    PILOT, LOGISTICIAN
}
