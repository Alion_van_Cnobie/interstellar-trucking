package interstellarTrucking.model.entities.enums;

public enum OrderStatus {
    CREATED, TRUCK_ASSIGNED, READY, PROCESSING, DONE, INTERRUPTED
}
