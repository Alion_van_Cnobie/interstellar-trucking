package interstellarTrucking.model.entities.enums;

public enum CheckpointType {
    LOAD, UNLOAD, TRANSIT
}
