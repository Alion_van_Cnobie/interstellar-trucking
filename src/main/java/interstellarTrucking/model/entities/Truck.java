package interstellarTrucking.model.entities;


import interstellarTrucking.model.entities.enums.TruckStatus;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "trucks")
public class Truck {

    private Integer id;
    private String number;
    private Integer shiftSize;
    private Integer capacity;
    private TruckStatus status;
    private Location location;
    private Boolean valid;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @Column(name = "number", nullable = false, unique = true)
    public String getNumber() {
        return number;
    }

    @Column(name = "shift_size", nullable = false)
    public Integer getShiftSize() {
        return shiftSize;
    }

    @Column(name = "capacity", nullable = false)
    public Integer getCapacity() {
        return capacity;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "status", nullable = false)
    public TruckStatus getStatus() {
        return status;
    }

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    public Location getLocation() {
        return location;
    }

    @Column(name = "valid")
    public Boolean getValid() {
        return valid;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setShiftSize(Integer shiftSize) {
        this.shiftSize = shiftSize;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public void setStatus(TruckStatus status) {
        this.status = status;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Truck)) return false;
        Truck truck = (Truck) o;
        return Objects.equals(id, truck.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
