package interstellarTrucking.model.entities;


import interstellarTrucking.model.entities.enums.UserRole;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    private Integer id;
    private String login;
    private String password;
    private UserRole role;
    private Boolean enabled;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @Column(name = "login", nullable = false, length = 45, unique = true)
    public String getLogin() {
        return login;
    }

    @Column(name = "password", nullable = false, length = 128)
    public String getPassword() {
        return password;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "role", nullable = false)
    public UserRole getRole() {
        return role;
    }

    @Column(name = "enabled", nullable = false)
    public Boolean getEnabled() {
        return enabled;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLogin(String username) {
        this.login = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
