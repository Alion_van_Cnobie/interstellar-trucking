package interstellarTrucking.model.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "hyperways")
public class Hyperway implements Serializable {

    private Location start;
    private Location finish;
    private Integer distance;


    @Id
    @ManyToOne
    @JoinColumn(name = "start_location_id", nullable = false)
    public Location getStart() {
        return start;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "end_location_id", nullable = false)
    public Location getFinish() {
        return finish;
    }

    @Column(name = "distance")
    public Integer getDistance() {
        return distance;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public void setFinish(Location end) {
        this.finish = end;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hyperway)) return false;
        Hyperway hyperway = (Hyperway) o;
        return Objects.equals(start, hyperway.start) &&
                Objects.equals(finish, hyperway.finish);
    }

    @Override
    public int hashCode() {

        return Objects.hash(start, finish);
    }
}
