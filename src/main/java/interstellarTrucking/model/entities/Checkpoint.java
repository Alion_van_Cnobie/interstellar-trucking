package interstellarTrucking.model.entities;



import interstellarTrucking.model.entities.enums.CheckpointType;

import javax.persistence.*;

@Entity
@Table(name = "checkpoints")
public class Checkpoint {

    private Integer id;
    private Location location;
    private Cargo cargo;
    private CheckpointType type;
    private Order order;
    private Integer position;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "location_id", nullable = false)
    public Location getLocation() {
        return location;
    }

    @ManyToOne
    @JoinColumn(name = "cargo_id")
    public Cargo getCargo() {
        return cargo;
    }

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type", nullable = false)
    public CheckpointType getType() {
        return type;
    }

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    public Order getOrder() {
        return order;
    }

    @Column(name = "position")
    public Integer getPosition() {
        return position;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public void setType(CheckpointType type) {
        this.type = type;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

}
