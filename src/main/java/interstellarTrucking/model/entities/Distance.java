package interstellarTrucking.model.entities;



import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "distances")
public class Distance implements Serializable, Comparable<Distance> {
    private Location start;
    private Location finish;
    private Location prev;
    private Integer distance;
    private Integer bypassNumber;

    @Id
    @ManyToOne
    @JoinColumn(name = "start_location_id", nullable = false)
    public Location getStart() {
        return start;
    }

    @Id
    @ManyToOne
    @JoinColumn(name = "end_location_id", nullable = false)
    public Location getFinish() {
        return finish;
    }

    @ManyToOne
    @JoinColumn(name = "prev_location_id", nullable = false)
    public Location getPrev() {
        return prev;
    }

    @Column(name = "distance", nullable = false)
    public Integer getDistance() {
        return distance;
    }

    @Column(name = "number")
    public Integer getBypassNumber() {
        return bypassNumber;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public void setFinish(Location end) {
        this.finish = end;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public void setBypassNumber(Integer number) {
        this.bypassNumber = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Distance)) return false;
        Distance distance = (Distance) o;
        return Objects.equals(start, distance.start) &&
                Objects.equals(finish, distance.finish);
    }

    @Override
    public int hashCode() {

        return Objects.hash(start, finish);
    }


    @Override
    public int compareTo(Distance distance) {
        return this.distance - distance.distance;
    }

    public void setPrev(Location prev) {
        this.prev = prev;
    }
}
