package interstellarTrucking.DAO.interfaces;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenericDAO<T> {

    void add (T entity);

    void update (T entity);

    void delete (T entity);

    List<T> getAll ();

    T getById (Integer id);
}
