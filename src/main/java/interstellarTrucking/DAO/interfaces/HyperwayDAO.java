package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Hyperway;
import interstellarTrucking.model.entities.Location;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HyperwayDAO {

    List<Hyperway> getAllByStartLocation(Location location);

    List<Hyperway> getAllByEndLocation(Location location);

    Hyperway getById(Location start, Location finish);

    List<Hyperway> getAll();
}
