package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Order;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.OrderStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDAO<T extends Order> extends GenericDAO<T> {

    Order getByNumber(String number);

    List<Order> getByTruck(Truck truck);

    List<Order> getAllWithStatus(OrderStatus status);

    List<Order> getAllWithoutStatus(OrderStatus status);
}
