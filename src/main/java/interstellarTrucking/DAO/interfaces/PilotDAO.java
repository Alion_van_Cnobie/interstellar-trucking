package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Pilot;
import interstellarTrucking.model.entities.User;
import interstellarTrucking.model.entities.enums.PilotStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PilotDAO<T extends Pilot> extends GenericDAO<T> {

    Pilot getByNumber(String number);

    List<Pilot> getAllValidWithStatus(PilotStatus status);

    List<Pilot> getAllFreeByLocation(Location location);

    Pilot getByUser(User user);

    List<Pilot> getAllValid();

    List<Pilot> getAllValidWithoutStatus(PilotStatus status);
}
