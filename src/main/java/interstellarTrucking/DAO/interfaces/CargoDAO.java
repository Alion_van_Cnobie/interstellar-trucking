package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Cargo;
import interstellarTrucking.model.entities.enums.CargoStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CargoDAO<T extends Cargo> extends GenericDAO<T> {
    List<Cargo> getAllProcessing();

    List<Cargo> getAllWithStatus(CargoStatus status);

    Cargo getByNumber(String number);
}
