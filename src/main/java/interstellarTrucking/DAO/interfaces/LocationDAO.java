package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Location;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationDAO<T extends Location> extends GenericDAO<T> {
    Location getByName(String name);
}
