package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO<T extends User> extends GenericDAO<T> {

    User getByUsername(String username);

}
