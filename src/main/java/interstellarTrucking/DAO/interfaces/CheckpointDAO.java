package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Checkpoint;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CheckpointDAO<T extends Checkpoint> extends GenericDAO<T> {

    List<Checkpoint> getAllByOrder(Order order);

    Checkpoint getByOrderAndPosition(Order order, Integer position);

    Checkpoint getByOrderAndLocation(Order order, Location location);
}
