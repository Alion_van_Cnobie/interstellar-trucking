package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.TruckStatus;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TruckDAO<T extends Truck> extends GenericDAO<T> {

    Truck getByNumber(String number);

    List<Truck> getAllValidWithStatus(TruckStatus status);

    List<Truck> getAllValid();

    List<Truck> getAllReadyWithCapacity(Integer capacity);
}
