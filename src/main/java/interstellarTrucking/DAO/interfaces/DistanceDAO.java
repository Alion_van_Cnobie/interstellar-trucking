package interstellarTrucking.DAO.interfaces;

import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Distance;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistanceDAO {

    void add(Distance distance);

    void update(Distance distance);

    List<Distance> getAllByStartLocation(Location location);

    List<Distance> getAllByEndLocation(Location location);

    Distance getCalculatedDistance(Location start, Location end);

    Distance getById(Location start, Location end);

    List<Distance> getCalculated(Location start);

    List<Distance> getUncalculated(Location start);

    List<Distance> getAll();
}
