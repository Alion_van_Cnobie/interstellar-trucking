package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.OrderDAO;
import interstellarTrucking.model.entities.Order;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.OrderStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class OrderDAOImpl<T extends Order> extends GenericDAOImpl<T> implements OrderDAO<T> {
    @Override
    public Order getByNumber(String number) {
        return (Order) sessionFactory.getCurrentSession()
                .createQuery("from Order where uniqueNumber = :number")
                .setParameter("number", number)
                .uniqueResult();
    }

    @Override
    public List<Order> getByTruck(Truck truck) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Order where truck = :truck order by id desc")
                .setParameter("truck", truck)
                .getResultList();
    }

    @Override
    public List<Order> getAllWithStatus(OrderStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Order where status = :status")
                .setParameter("status", status)
                .getResultList();
    }

    @Override
    public List<Order> getAllWithoutStatus(OrderStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Order where status != :status order by id desc")
                .setParameter("status", status)
                .getResultList();
    }

}
