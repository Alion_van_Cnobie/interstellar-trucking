package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.HyperwayDAO;
import interstellarTrucking.model.entities.Hyperway;
import interstellarTrucking.model.entities.Location;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class HyperwayDAOImpl implements HyperwayDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public HyperwayDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Hyperway> getAllByStartLocation(Location location) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Hyperway where start = :location")
                .setParameter("location", location)
                .getResultList();
    }

    @Override
    public List<Hyperway> getAllByEndLocation(Location location) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Hyperway where finish = :location")
                .setParameter("location", location)
                .getResultList();
    }

    @Override
    public Hyperway getById(Location start, Location finish) {
        return (Hyperway) sessionFactory.getCurrentSession()
                .createQuery("from Hyperway where start = :start and finish = :finish")
                .setParameter("start", start)
                .setParameter("finish", finish)
                .uniqueResult();
    }

    @Override
    public List<Hyperway> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Hyperway")
                .getResultList();
    }
}
