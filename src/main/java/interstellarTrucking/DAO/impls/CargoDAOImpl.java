package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.CargoDAO;
import interstellarTrucking.model.entities.Cargo;
import interstellarTrucking.model.entities.enums.CargoStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CargoDAOImpl<T extends Cargo> extends GenericDAOImpl<T> implements CargoDAO<T> {

    @Override
    public List<Cargo> getAllProcessing() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Cargo where status = :ready or status = :processing or status = :loaded or status = :created")
                .setParameter("ready", CargoStatus.READY)
                .setParameter("processing", CargoStatus.PROCESSING)
                .setParameter("loaded", CargoStatus.LOADED)
                .setParameter("created", CargoStatus.NEW)
                .getResultList();

    }

    @Override
    public List<Cargo> getAllWithStatus(CargoStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Cargo where status = :status")
                .setParameter("status", status)
                .getResultList();

    }

    @Override
    public Cargo getByNumber(String number) {
        return (Cargo) sessionFactory.getCurrentSession()
                .createQuery("from Cargo where number = :number")
                .setParameter("number", number)
                .uniqueResult();
    }
}
