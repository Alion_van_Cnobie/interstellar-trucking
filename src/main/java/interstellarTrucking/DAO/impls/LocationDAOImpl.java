package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.model.entities.Location;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class LocationDAOImpl<T extends Location> extends GenericDAOImpl<T> implements LocationDAO<T> {
    @Override
    public void add(T entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(T entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(T entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Location getByName(String name) {
        return (Location) sessionFactory.getCurrentSession()
                .createQuery("from Location where name = :name")
                .setParameter("name", name)
                .uniqueResult();
    }
}
