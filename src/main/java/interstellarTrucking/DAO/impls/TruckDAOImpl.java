package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.TruckDAO;
import interstellarTrucking.model.entities.Truck;
import interstellarTrucking.model.entities.enums.TruckStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TruckDAOImpl<T extends Truck> extends GenericDAOImpl<T> implements TruckDAO<T> {

    @Override
    public Truck getByNumber(String number) {
        return (Truck) sessionFactory.getCurrentSession()
                .createQuery("from Truck where number = :number")
                .setParameter("number", number)
                .uniqueResult();
    }

    @Override
    public List<Truck> getAllValidWithStatus(TruckStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Truck where status = :status and valid = :valid")
                .setParameter("status", status)
                .setParameter("valid", true)
                .getResultList();
    }

    @Override
    public List<Truck> getAllValid() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Truck where valid = :valid")
                .setParameter("valid", true)
                .getResultList();
    }

    @Override
    public List<Truck> getAllReadyWithCapacity(Integer capacity) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Truck where status = :status and capacity >= :capacity order by shiftSize")
                .setParameter("status", TruckStatus.READY)
                .setParameter("capacity", capacity)
                .getResultList();
    }
}
