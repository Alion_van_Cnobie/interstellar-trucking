package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.CheckpointDAO;
import interstellarTrucking.model.entities.Checkpoint;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class CheckpointDAOImpl<T extends Checkpoint> extends GenericDAOImpl<T> implements CheckpointDAO<T> {

    @Override
    public List<Checkpoint> getAllByOrder(Order order) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Checkpoint where order = :order order by position")
                .setParameter("order", order)
                .getResultList();
    }

    @Override
    public Checkpoint getByOrderAndPosition(Order order, Integer position) {
        return (Checkpoint) sessionFactory.getCurrentSession()
                .createQuery("from Checkpoint  where order = :order and position = :position")
                .setParameter("order", order)
                .setParameter("position", position)
                .uniqueResult();
    }

    @Override
    public Checkpoint getByOrderAndLocation(Order order, Location location) {
        return (Checkpoint) sessionFactory.getCurrentSession()
                .createQuery("from Checkpoint  where order = :order and location = :location")
                .setParameter("order", order)
                .setParameter("location", location)
                .uniqueResult();
    }


}
