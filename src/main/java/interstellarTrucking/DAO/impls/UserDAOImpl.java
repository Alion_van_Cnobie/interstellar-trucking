package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.UserDAO;
import interstellarTrucking.model.entities.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserDAOImpl<T extends User> extends GenericDAOImpl<T> implements UserDAO<T> {

    @Autowired
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getByUsername(String username) {
        return (User) sessionFactory.getCurrentSession()
                .createQuery("from User where login = :username")
                .setParameter("username", username)
                .uniqueResult();
    }

}
