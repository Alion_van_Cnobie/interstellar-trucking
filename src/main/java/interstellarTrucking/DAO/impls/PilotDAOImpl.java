package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.PilotDAO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Pilot;
import interstellarTrucking.model.entities.User;
import interstellarTrucking.model.entities.enums.PilotStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class PilotDAOImpl<T extends Pilot> extends GenericDAOImpl<T> implements PilotDAO<T> {

    @Override
    public Pilot getByNumber(String number) {
        return (Pilot) sessionFactory.getCurrentSession()
                .createQuery("from Pilot where number = :number")
                .setParameter("number", number)
                .uniqueResult();
    }

    @Override
    public List<Pilot> getAllValidWithStatus(PilotStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Pilot where status = :status and valid = :valid")
                .setParameter("status", status)
                .setParameter("valid", true)
                .getResultList();
    }

    @Override
    public List<Pilot> getAllFreeByLocation(Location location) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Pilot where location = :location and status = :status and valid = :valid order by workedOut")
                .setParameter("status", PilotStatus.FREE)
                .setParameter("location", location)
                .setParameter("valid", true)
                .getResultList();
    }

    @Override
    public Pilot getByUser(User user) {
        return (Pilot) sessionFactory.getCurrentSession()
                .createQuery("from Pilot where user = :user")
                .setParameter("user", user)
                .uniqueResult();
    }

    @Override
    public List<Pilot> getAllValid() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Pilot where valid = :valid")
                .setParameter("valid", true)
                .getResultList();
    }

    @Override
    public List<Pilot> getAllValidWithoutStatus(PilotStatus status) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Pilot where status != :status and valid = :valid")
                .setParameter("status", status)
                .setParameter("valid", true)
                .getResultList();
    }
}
