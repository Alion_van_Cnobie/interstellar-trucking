package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.GenericDAO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public abstract class GenericDAOImpl<T> implements GenericDAO<T> {

    private Class<T> entityClass;

    @Autowired
    protected SessionFactory sessionFactory;

    public GenericDAOImpl() {
        this.entityClass = (Class<T>) GenericTypeResolver.resolveTypeArgument(getClass(), GenericDAO.class);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void add(T entity) {
        sessionFactory.getCurrentSession()
                .save(entity);
    }

    public void update(T entity) {
        sessionFactory.getCurrentSession()
                .update(entity);
    }

    public void delete(T entity) {
        sessionFactory.getCurrentSession()
                .delete(entity);
    }

    public List<T> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("from " + entityClass.getName())
                .getResultList();
    }

    public T getById(Integer id) {
        return sessionFactory.getCurrentSession()
                .get(entityClass, id);
    }
}
