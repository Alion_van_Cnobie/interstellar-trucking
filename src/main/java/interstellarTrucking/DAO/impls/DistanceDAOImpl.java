package interstellarTrucking.DAO.impls;

import interstellarTrucking.DAO.interfaces.DistanceDAO;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.Distance;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class DistanceDAOImpl implements DistanceDAO {

    private final SessionFactory sessionFactory;

    @Autowired
    public DistanceDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(Distance distance) {
        sessionFactory.getCurrentSession().save(distance);
    }

    @Override
    public void update(Distance distance) {
        sessionFactory.getCurrentSession()
                .update(distance);
    }

    @Override
    public List<Distance> getAllByStartLocation(Location location) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Distance where start = :location")
                .setParameter("location", location)
                .getResultList();
    }

    @Override
    public List<Distance> getAllByEndLocation(Location location) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Distance where finish = :location")
                .setParameter("location", location)
                .getResultList();
    }

    @Override
    public Distance getCalculatedDistance(Location start, Location finish) {
        return (Distance) sessionFactory.getCurrentSession()
                .createQuery("from Distance where start = :start and finish = :finish and bypassNumber != null ")
                .setParameter("start", start)
                .setParameter("finish", finish)
                .uniqueResult();
    }

    @Override
    public Distance getById(Location start, Location finish) {
        return (Distance) sessionFactory.getCurrentSession()
                .createQuery("from Distance where start = :start and finish = :finish")
                .setParameter("start", start)
                .setParameter("finish", finish)
                .uniqueResult();
    }

    @Override
    public List<Distance> getCalculated(Location start) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Distance where start = :start and bypassNumber != null order by bypassNumber desc")
                .setParameter("start" , start)
                .getResultList();
    }

    @Override
    public List<Distance> getUncalculated(Location start) {
        return sessionFactory.getCurrentSession()
                .createQuery("from Distance where start = :start and bypassNumber = null order by distance")
                .setParameter("start", start)
                .getResultList();
    }

    @Override
    public List<Distance> getAll() {
        return sessionFactory.getCurrentSession()
                .createQuery("from Distance")
                .getResultList();
    }
}
