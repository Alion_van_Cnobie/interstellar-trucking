package interstellarTrucking.controller;

import interstellarTrucking.model.DTO.*;
import interstellarTrucking.model.entities.Checkpoint;
import interstellarTrucking.model.entities.Order;
import interstellarTrucking.model.entities.enums.OrderStatus;
import interstellarTrucking.model.service.interfaces.CheckpointService;
import interstellarTrucking.model.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/logistician")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CheckpointService checkpointService;

    @GetMapping("/orders")
    public ModelAndView show() {
        List<OrderDTO> orderDTOList = orderService.convertEntityListToDTO(orderService.getAllWithStatus(OrderStatus.PROCESSING));
        return new ModelAndView("orders", "orderList", orderDTOList);
    }

    @GetMapping("/orders/{number}")
    public ModelAndView showOrder(@PathVariable String number) {
        Order order = orderService.getByNumber(number);
        List<Checkpoint> checkpoints = checkpointService.getAllByOrder(order);
        return new ModelAndView("order-page", "order", orderService.convertEntityToDTO(order))
                .addObject("checkpoints", checkpoints);
    }
}
