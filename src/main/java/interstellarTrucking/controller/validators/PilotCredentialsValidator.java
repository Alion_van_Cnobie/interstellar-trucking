package interstellarTrucking.controller.validators;

import interstellarTrucking.model.DTO.PilotCredentials;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PilotCredentialsValidator implements Validator {
    private static int PASSWORD_MIN_LENGTH = 8;

    @Override
    public boolean supports(Class<?> aClass) {
        return PilotCredentials.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "usernameEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "passwordEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirm", "confirmEmpty");

        if (!errors.hasErrors()) {
            rejectIfTooShort(errors, "password", "shortPassword", PASSWORD_MIN_LENGTH);
        }

        if (!errors.hasErrors()) {
            rejectIfNotMatch(errors, "password", "confirm", "unconfirmedPassword");
        }
    }

    private void rejectIfTooShort(Errors errors, String field, String errorCode, int minLength) {
        Object value = errors.getFieldValue(field);

        if (value.toString().length() < minLength) {
            errors.rejectValue(field, errorCode);
        }
    }

    private void rejectIfNotMatch(Errors errors, String field1, String field2, String errorCode) {
        Object value1 = errors.getFieldValue(field1);
        Object value2 = errors.getFieldValue(field1);

        if (!value1.toString().equals(value2.toString())) {
            errors.rejectValue(field1, errorCode);
            errors.rejectValue(field2, errorCode);
        }
    }
}
