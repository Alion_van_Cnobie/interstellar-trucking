package interstellarTrucking.controller.validators;

import interstellarTrucking.model.DTO.TruckDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TruckValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return TruckDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number", "numberEmpty");
        CommonValidator.rejectIfWrongFormat(errors, "number", "wrongFormat");
        CommonValidator.rejectIfLessThanNumber(errors, "shiftSize", "lessThanTwo", 2);
        CommonValidator.rejectIfLessThanNumber(errors, "capacity", "lessThan10000", 10000);
    }
}
