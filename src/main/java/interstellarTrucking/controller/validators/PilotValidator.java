package interstellarTrucking.controller.validators;

import interstellarTrucking.model.DTO.PilotDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PilotValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PilotDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstNameEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastNameEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "workedOut", "workedOutEmpty");
        CommonValidator.rejectIfContainsNotLetters(errors, "firstName", "firstNameSpoiled");
        CommonValidator.rejectIfContainsNotLetters(errors, "lastName", "lastNameSpoiled");
    }
}
