package interstellarTrucking.controller.validators;

import org.springframework.validation.Errors;


public abstract class CommonValidator {

    public static void rejectIfContainsNotLetters(Errors errors, String field, String errorCode) {
        Object value = errors.getFieldValue(field);
        if (!value.toString().chars().allMatch(Character::isLetter)) {
            errors.rejectValue(field, errorCode);
        }
    }

    public static void rejectIfWrongFormat(Errors errors, String field, String errorCode) {
        Object value = errors.getFieldValue(field);
        if (!value.toString().matches("^[A-Z]{2}[0-9]{5}$")
                 && !value.toString().isEmpty()) {
            errors.rejectValue(field, errorCode);
        }
    }

    public static void rejectIfLessThanNumber(Errors errors, String field, String errorCode, int threshold) {
        Object value = errors.getFieldValue(field);
        if (value == null || Integer.parseInt(value.toString().trim()) < threshold) {
            errors.rejectValue(field, errorCode);
        }
    }
}
