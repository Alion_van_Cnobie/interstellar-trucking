package interstellarTrucking.controller.validators;

import interstellarTrucking.model.DTO.CargoDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CargoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return CargoDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "nameEmpty");
        CommonValidator.rejectIfLessThanNumber(errors, "weight", "lessThanOne", 1);
        CargoDTO cargoDTO = (CargoDTO)o;
        if (cargoDTO.getStart().equals(cargoDTO.getEnd())) {
            errors.rejectValue("start", "sameLocations");
            errors.rejectValue("end", "sameLocations");
        }
    }


}
