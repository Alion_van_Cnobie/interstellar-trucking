package interstellarTrucking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class LogisticianController {

    @GetMapping("/logistician")
    public String logistician() {
        return "logistician";
    }
}
