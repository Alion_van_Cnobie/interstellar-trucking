package interstellarTrucking.controller;

import interstellarTrucking.model.DTO.PilotDTO;
import interstellarTrucking.controller.validators.PilotValidator;
import interstellarTrucking.model.entities.enums.PilotStatus;
import interstellarTrucking.model.service.interfaces.LocationService;
import interstellarTrucking.model.service.interfaces.PilotService;
import interstellarTrucking.model.service.interfaces.TruckBoardNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/logistician")
public class PilotsController {

    @Autowired
    private PilotService pilotService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private PilotValidator pilotValidator;

    @Autowired
    private TruckBoardNotifier truckBoardNotifier;

    @GetMapping("/pilots")
    public ModelAndView showPilots() {
        List<PilotDTO> pilotDTOList = pilotService.convertEntityListToDTO(pilotService.getAllValid());
        return new ModelAndView("pilots", "pilotList", pilotDTOList)
                .addObject("formEnabled", false)
                .addObject("pilot", new PilotDTO())
                .addObject("locations", locationService.getAll());
    }

    @GetMapping("/pilots/{number}")
    public ModelAndView showPilot(@PathVariable String number) {
        PilotDTO pilotDTO = pilotService.getByNumber(number);
        List<PilotStatus> statuses = Arrays.asList(PilotStatus.values());
        if (pilotDTO == null) return new ModelAndView("redirect:/logistician/pilots");
        return new ModelAndView("pilot-page", "pilot", pilotDTO)
                .addObject("statuses", statuses)
                .addObject("pilotUpd", new PilotDTO())
                .addObject("formStatus", 0)
                .addObject("locations", locationService.getAll());
    }

    @GetMapping("/pilots/{number}/update")
    public ModelAndView showUpdateForm(@PathVariable String number) {
        return showPilot(number).addObject("formStatus", 1);
    }

    @GetMapping("/pilots/{number}/delete")
    public ModelAndView showDeleteForm(@PathVariable String number) {
        return showPilot(number).addObject("formStatus", 2);
    }

    @PostMapping("/pilots")
    public ModelAndView newPilot(@ModelAttribute("pilot") PilotDTO pilotDTO, BindingResult result) {
        pilotValidator.validate(pilotDTO, result);
        if (result.hasErrors()) {
            return showPilots()
                    .addObject("pilot", pilotDTO)
                    .addObject("formEnabled", true);
        }

        String number = pilotService.add(pilotDTO);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/pilots/" + number);
    }

    @PostMapping("/pilots/{number}/update")
    public ModelAndView update(@PathVariable String number, @ModelAttribute("pilotUpd") PilotDTO pilotDTO, BindingResult result) {
        pilotValidator.validate(pilotDTO, result);
        if(result.hasErrors()) {
            return showUpdateForm(number).addObject("pilotUpd", pilotDTO);
        }

        pilotService.update(number, pilotDTO);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/pilots/" + number);
    }

    @PostMapping("/pilots/{number}/delete")
    public ModelAndView delete(@PathVariable String number) {
        pilotService.delete(number);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/pilots/" + number);
    }
}
