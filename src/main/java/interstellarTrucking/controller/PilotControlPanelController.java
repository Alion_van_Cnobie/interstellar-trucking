package interstellarTrucking.controller;

import interstellarTrucking.controller.validators.PilotCredentialsValidator;
import interstellarTrucking.model.DTO.PilotCredentials;
import interstellarTrucking.model.DTO.PilotInfoDTO;
import interstellarTrucking.model.service.interfaces.PilotControlService;
import interstellarTrucking.model.service.interfaces.TruckBoardNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/pilot")
public class PilotControlPanelController {

    @Autowired
    PilotControlService pilotControlService;

    @Autowired
    PilotCredentialsValidator validator;

    @Autowired
    private TruckBoardNotifier truckBoardNotifier;

    @GetMapping
    public ModelAndView pilot() {
        PilotInfoDTO pilotInfo = pilotControlService.getCurrentPilotInfo();
        if (pilotInfo == null) {
            return new ModelAndView("redirect:/pilot/confirm");
        }
        return new ModelAndView("pilot", "pilotInfo", pilotInfo);
    }

    @GetMapping("/confirm")
    public ModelAndView confirm() {
        return new ModelAndView("new-credentials", "credentials", new PilotCredentials());
    }

    @PostMapping("/confirm")
    public ModelAndView confirm(@ModelAttribute PilotCredentials credentials, BindingResult result) {
        validator.validate(credentials, result);

        if (result.hasErrors()) {
            return confirm().addObject("credentials", credentials);
        }

        if (!pilotControlService.updateCredentials(credentials)) {
            result.rejectValue("username", "userExists");
            return confirm().addObject("credentials", credentials);
        }

        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/login");
    }

    @PostMapping("/{action}")
    public ModelAndView process(@PathVariable String action) {
        switch (action) {
            case "start" : pilotControlService.getWork();
            break;
            case "ready" : pilotControlService.setReady();
            break;
            case "dayoff" : pilotControlService.setDayoff();
            break;
            case "control" : pilotControlService.takeControl();
            break;
            case "startload" : pilotControlService.startLoading();
            break;
            case "finishload" : pilotControlService.finishLoading();
            break;
            case "hyperdrive" : pilotControlService.nextLocation();
            break;
            case "broken" : pilotControlService.setBroken();
            break;
            case "finishwork" : pilotControlService.finishWork();
            break;
        }

        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/pilot");
    }

}
