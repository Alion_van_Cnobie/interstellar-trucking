package interstellarTrucking.controller;

import interstellarTrucking.model.DTO.TruckDTO;
import interstellarTrucking.controller.validators.TruckValidator;
import interstellarTrucking.model.entities.enums.TruckStatus;
import interstellarTrucking.model.service.interfaces.LocationService;
import interstellarTrucking.model.service.interfaces.TruckBoardNotifier;
import interstellarTrucking.model.service.interfaces.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/logistician")
public class TrucksController {
    @Autowired
    private TruckService truckService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private TruckValidator validator;

    @Autowired
    private TruckBoardNotifier truckBoardNotifier;

    @GetMapping("/trucks")
    public ModelAndView showTrucks() {
        List<TruckDTO> truckDTOList = truckService.convertEntityListToDTO(truckService.getAllValid());
        return new ModelAndView("trucks", "truckList", truckDTOList)
                .addObject("formEnabled", false)
                .addObject("truck", new TruckDTO())
                .addObject("locations", locationService.getAll());
    }

    @GetMapping("/trucks/{number}")
    public ModelAndView showTruck(@PathVariable String number) {
        TruckDTO truckDTO = truckService.getByNumber(number);
        List<TruckStatus> statuses = Arrays.asList(TruckStatus.values());
        if (truckDTO == null) return new ModelAndView("redirect:/logistician/trucks");
        return new ModelAndView("truck-page", "truck", truckDTO)
                .addObject("statuses", statuses)
                .addObject("locations", locationService.getAll())
                .addObject("truckUpd", new TruckDTO())
                .addObject("formStatus", 0);
    }

    @GetMapping("/trucks/{number}/update")
    public ModelAndView showUpdateForm(@PathVariable String number) {
        return showTruck(number).addObject("formStatus", 1);
    }

    @GetMapping("/trucks/{number}/delete")
    public ModelAndView showDeleteForm(@PathVariable String number) {
        return showTruck(number).addObject("formStatus", 2);
    }

    @PostMapping("/trucks")
    public ModelAndView newTruck(@ModelAttribute("truck") TruckDTO truckDTO, BindingResult result) {
       validator.validate(truckDTO, result);
        if (result.hasErrors()) {
            return showTrucks()
                    .addObject("truck", truckDTO)
                    .addObject("formEnabled", true);
        }

        if (!truckService.add(truckDTO)) {
            result.rejectValue("number", "truckExists");
            return showTrucks()
                    .addObject("truck", truckDTO)
                    .addObject("formEnabled", true);
        }
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/trucks/" + truckDTO.getNumber());
    }

    @PostMapping("/trucks/{number}/update")
    public ModelAndView update(@PathVariable String number, @ModelAttribute("truckUpd") TruckDTO truckDTO, BindingResult result) {
        validator.validate(truckDTO, result);
        if(result.hasErrors()) {
            return showUpdateForm(number).addObject("truckUpd", truckDTO);
        }

        truckService.update(number, truckDTO);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/trucks/" + number);
    }

    @PostMapping("/trucks/{number}/delete")
    public ModelAndView delete(@PathVariable String number) {
        truckService.delete(number);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/trucks/" + number);
    }
}

