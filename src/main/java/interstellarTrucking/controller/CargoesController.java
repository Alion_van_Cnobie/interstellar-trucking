package interstellarTrucking.controller;

import interstellarTrucking.controller.validators.CargoValidator;
import interstellarTrucking.model.DTO.CargoDTO;
import interstellarTrucking.model.entities.enums.CargoStatus;
import interstellarTrucking.model.service.interfaces.CargoService;
import interstellarTrucking.model.service.interfaces.LocationService;
import interstellarTrucking.model.service.interfaces.TruckBoardNotifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/logistician")
public class CargoesController {

    @Autowired
    private CargoService cargoService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private CargoValidator validator;

    @Autowired
    private TruckBoardNotifier truckBoardNotifier;

    @GetMapping("/cargoes")
    public ModelAndView show() {
        List<CargoDTO> cargoDTOList = cargoService.convertEntityListToDTO(cargoService.getAllProcessing());
        return new ModelAndView("cargoes", "cargoList", cargoDTOList)
                .addObject("cargo", new CargoDTO())
                .addObject("locations", locationService.getAll())
                .addObject("formEnabled", false);
    }

    @GetMapping("/cargoes/{number}")
    public ModelAndView showCargo(@PathVariable String number) {
        CargoDTO cargoDTO = cargoService.getByNumber(number);
        List<CargoStatus> statuses = Arrays.asList(CargoStatus.values());
        if (cargoDTO == null) return new ModelAndView("redirect:/logistician/cargoes");
        return new ModelAndView("cargo-page", "cargo", cargoDTO)
                .addObject("statuses", statuses)
                .addObject("locations", locationService.getAll())
                .addObject("cargoUpd", new CargoDTO())
                .addObject("formStatus", 0);
    }

    @GetMapping("/cargoes/{number}/update")
    public ModelAndView showUpdateForm(@PathVariable String number) {
        return showCargo(number).addObject("formStatus", 1);
    }

    @GetMapping("/cargoes/{number}/delete")
    public ModelAndView showDeleteForm(@PathVariable String number) {
        return showCargo(number).addObject("formStatus", 2);
    }

    @PostMapping("/cargoes")
    public ModelAndView newCargo(@ModelAttribute("cargo") CargoDTO cargoDTO, BindingResult result) {
        validator.validate(cargoDTO, result);
        if (result.hasErrors()) {
            return show()
                    .addObject("cargo", cargoDTO)
                    .addObject("formEnabled", true);
        }
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/cargoes/" + cargoService.add(cargoDTO));
    }

    @PostMapping("/cargoes/{number}/update")
    public ModelAndView update(@PathVariable String number, @ModelAttribute("cargoUpd") CargoDTO cargoDTO, BindingResult result) {
        validator.validate(cargoDTO, result);
        if(result.hasErrors()) {
            return showUpdateForm(number).addObject("cargoUpd", cargoDTO);
        }
        cargoDTO.setNumber(number);
        cargoService.update(cargoDTO);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/cargoes/" + number);
    }

    @PostMapping("/cargoes/{number}/delete")
    public ModelAndView delete(@PathVariable String number) {
        cargoService.delete(number);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/cargoes/" + number);
    }

    @PostMapping("cargoes/{number}/send")
    public ModelAndView send(@PathVariable String number) {
        cargoService.send(number);
        truckBoardNotifier.sendNotice("Something new appeared!");
        return new ModelAndView("redirect:/logistician/cargoes/" + number);
    }
}


