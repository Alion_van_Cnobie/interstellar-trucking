package interstellarTrucking.controller;

import interstellarTrucking.model.DTO.BoardDTO;
import interstellarTrucking.model.service.interfaces.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BoardController {

    @Autowired
    private BoardService boardService;

    @GetMapping("/board")
    public BoardDTO getData() {
        return boardService.getBoardDTO();
    }
}
