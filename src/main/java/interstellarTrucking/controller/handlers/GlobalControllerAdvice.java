package interstellarTrucking.controller.handlers;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice(basePackages = {"interstellarTrucking.controller"})
public class GlobalControllerAdvice {

    private final Logger logger = Logger.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    public ModelAndView exceptionHandler(Exception ex) {
        logger.error(ex + " | " + ex.getMessage() + " | " + ex.getStackTrace()[0]);
        return new ModelAndView("error", "message", "Oops... Something wrong happened!");
    }
}
