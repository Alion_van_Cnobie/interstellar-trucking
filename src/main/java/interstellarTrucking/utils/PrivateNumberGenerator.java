package interstellarTrucking.utils;

public class PrivateNumberGenerator {
    public static String generatePrivateNumber() {
        int random = (int)(Math.random()*9000000 + 1000000);
        return Integer.toString(random);
    }
}
