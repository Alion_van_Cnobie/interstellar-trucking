package JUnit;


import interstellarTrucking.config.AppConfig;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.service.interfaces.DistanceService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@Transactional
public class DistanceServiceTest {

    @Autowired
    private DistanceService distanceService;

    @Test
    public void testDistance1() {
        Integer expectedResult = 7;
        Location start = getLocation(1, "Sun");
        Location finish = getLocation(8, "Kathea");
        Assert.assertEquals(distanceService.getDistance(start, finish), expectedResult);
    }

    @Test
    public void testDistance2() {
        Integer expectedResult = 76;
        Location start = getLocation(33, "Thradd");
        Location finish = getLocation(15, "Jundas");
        Assert.assertEquals(distanceService.getDistance(start, finish), expectedResult);
    }

    @Test
    public void testDistance3() {
        Integer expectedResult = 65;
        Location start = getLocation(30, "Hannery");
        Location finish = getLocation(1, "Sun");
        Assert.assertEquals(distanceService.getDistance(start, finish), expectedResult);
    }

    @Test
    public void testDistance4() {
        Integer expectedResult = 64;
        Location start = getLocation(1, "Sun");
        Location finish = getLocation(30, "Hannery");
        Assert.assertEquals(distanceService.getDistance(start, finish), expectedResult);
    }

    @Test
    public void testRoute1() {
        Location start = getLocation(1, "Sun");
        Location finish = getLocation(8, "Kathea");
        List<Location> expected = new ArrayList<>();
        List<Location> result = distanceService.getRoute(start, finish);
        assertLists(result, expected);
    }

    @Test
    public void testRoute2() {
        Location start = getLocation(33, "Thradd");
        Location finish = getLocation(15, "Jundas");
        List<Location> expected = new ArrayList<>();
        expected.add(getLocation(24, "Mechaz"));
        expected.add(getLocation(23, "Talandra"));
        expected.add(getLocation(22, "Duhr"));
        expected.add(getLocation(17, "Nietz"));
        expected.add(getLocation(16, "Ekoecromia"));
        List<Location> result = distanceService.getRoute(start, finish);
        assertLists(result, expected);
    }

    private Location getLocation(Integer id, String name) {
        Location location = new Location();
        location.setId(id);
        location.setName(name);
        return location;
    }

    private void assertLists(List<?> result, List<?> expected) {
        Assert.assertEquals(result.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            Assert.assertEquals(result.get(i), expected.get(i));
        }
    }
}
