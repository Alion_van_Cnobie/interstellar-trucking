package JUnit;

import interstellarTrucking.DAO.interfaces.LocationDAO;
import interstellarTrucking.config.AppConfig;
import interstellarTrucking.model.entities.Cargo;
import interstellarTrucking.model.entities.Checkpoint;
import interstellarTrucking.model.entities.Location;
import interstellarTrucking.model.entities.enums.CargoStatus;
import interstellarTrucking.model.entities.enums.CheckpointType;
import interstellarTrucking.model.service.interfaces.CheckpointService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
@Transactional
public class CheckpointServiceTest {

    @Autowired
    private CheckpointService checkpointService;

    @Autowired
    private LocationDAO<Location> locationDAO;

    private Map<String, Location> locationMap = new HashMap<>();

    @Before
    public void initLocationMap() {
        List<Location> locations = locationDAO.getAll();

        for(Location location: locations) {
            locationMap.put(location.getName(), location);
        }
    }

    @Test
    public void createCheckpointTest1() {
        Cargo cargo = getCargo(locationMap.get("Sun"), locationMap.get("Deneb"));
        Checkpoint checkpoint = checkpointService.createCheckpoint(cargo, CheckpointType.TRANSIT);
        Assert.assertNull(checkpoint);
    }

    @Test
    public void createCheckpointTest2() {
        Location start = locationMap.get("Sun");
        Location finish = locationMap.get("Deneb");
        Cargo cargo = getCargo(start, finish);
        Checkpoint checkpoint = checkpointService.createCheckpoint(cargo, CheckpointType.LOAD);
        Assert.assertEquals(checkpoint.getLocation(), start);
    }

    @Test
    public void createCheckpointTest3() {
        Location start = locationMap.get("Sun");
        Location finish = locationMap.get("Deneb");
        Cargo cargo = getCargo(start, finish);
        Checkpoint checkpoint = checkpointService.createCheckpoint(cargo, CheckpointType.UNLOAD);
        Assert.assertEquals(checkpoint.getLocation(), finish);
    }

    private Cargo getCargo(Location start, Location finish) {
        Cargo cargo = new Cargo();
        cargo.setStart(start);
        cargo.setEnd(finish);
        cargo.setStatus(CargoStatus.READY);
        return cargo;
    }

}
